//
//  CustomViewCell.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 4/6/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CustomViewCell: JTAppleCell {
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var eventDotView: UIView!
}
