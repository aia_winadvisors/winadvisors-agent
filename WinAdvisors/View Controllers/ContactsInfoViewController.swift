//
//  ContactsInfoViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 23/6/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit
import SVProgressHUD
import NotificationCenter

class ContactsInfoViewController: UIViewController {
    
    
    @IBOutlet weak var icLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var mobileNoLabel: UILabel!
    @IBOutlet weak var AddressLabel: UILabel!
    @IBOutlet weak var clientInfoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverageInfoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var chartsInfoheightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var annualIncomeText: UILabel!
    @IBOutlet weak var deathBenefitsText: UILabel!
    @IBOutlet weak var deathOptText: UILabel!
    @IBOutlet weak var deathMet: UIImageView!
    @IBOutlet weak var tpdBenefitsText: UILabel!
    @IBOutlet weak var tpdOptText: UILabel!
    @IBOutlet weak var tpdMet: UIImageView!
    @IBOutlet weak var critIllBenefitsText: UILabel!
    @IBOutlet weak var critIllMet: UIImageView!
    @IBOutlet weak var critIllOptText: UILabel!
    @IBOutlet weak var earlyCritIllBenefitsText: UILabel!
    @IBOutlet weak var earlyCritIllMet: UIImageView!
    @IBOutlet weak var earlyCrtiIllOptText: UILabel!
    @IBOutlet weak var disabilityIncomeBenefitsText: UILabel!
    @IBOutlet weak var disabilityIncomeMet: UIImageView!
    @IBOutlet weak var accDeathText: UILabel!
    @IBOutlet weak var accDeathMet: UIImageView!
    @IBOutlet weak var accDeathOptText: UILabel!
    @IBOutlet weak var accTpdText: UILabel!
    @IBOutlet weak var accTpdMet: UIImageView!
    @IBOutlet weak var accTpdOptText: UILabel!
    @IBOutlet weak var accReimbursementText: UILabel!
    @IBOutlet weak var accReimbursementMet: UIImageView!
    @IBOutlet weak var accReimbursementOptText: UILabel!
    
    @IBOutlet weak var deathShortfall: UILabel!
    @IBOutlet weak var tpdShortfall:UILabel!
    @IBOutlet weak var critIllnessShortfall: UILabel!
    @IBOutlet weak var earlyCritIllnessShortfall: UILabel!
    @IBOutlet weak var disabilityIncomeShortfall: UILabel!
    @IBOutlet weak var accDeathShortfall: UILabel!
    @IBOutlet weak var accTpdShortfall: UILabel!
    @IBOutlet weak var accReimbursementShortfall: UILabel!
    
    
    private var benefits: Benefits! {
        didSet {
            // isHighlighted = when a category is not met
            print(benefits)
            deathBenefitsText.text = benefits.death
            deathMet.isHighlighted = !benefits.deathMet
            deathShortfall.text = benefits.deathShortFall
//            deathMet.tintColor = UIColor.black
            tpdBenefitsText.text = benefits.tpd
            tpdMet.isHighlighted = !benefits.tpdMet
            tpdShortfall.text = benefits.tpdShortFall
//            tpdMet.tintColor = UIColor.black
            critIllBenefitsText.text = benefits.critIll
            critIllMet.isHighlighted = !benefits.critIllMet
            critIllnessShortfall.text = benefits.critIllShortFall
//            critIllMet.tintColor = UIColor.black
            earlyCritIllBenefitsText.text = benefits.earlyCritIll
            earlyCritIllMet.isHighlighted = !benefits.earlyCritIllMet
            earlyCritIllnessShortfall.text = benefits.earlyCritIllShortFall
//            earlyCritIllMet.tintColor = UIColor.black
            disabilityIncomeBenefitsText.text = benefits.disabilityIncome
            disabilityIncomeMet.isHighlighted = !benefits.disabilityIncomeMet
            disabilityIncomeShortfall.text = benefits.disabilityIncomeShortFall
//            disabilityIncomeMet.tintColor = UIColor.black
            accDeathText.text = benefits.accDeath
            accDeathMet.isHighlighted = !benefits.accDeathMet
            accDeathShortfall.text = benefits.accDeathShortFall
//            accDeathMet.tintColor = UIColor.black
            accTpdText.text = benefits.accTpd
            accTpdMet.isHighlighted = !benefits.accTpdMet
            accTpdShortfall.text = benefits.accTpdShortFall
//            accTpdMet.tintColor = UIColor.black
            accReimbursementText.text = benefits.accReimbursement
            accReimbursementMet.isHighlighted = !benefits.accReimbursementMet
            accReimbursementShortfall.text = benefits.accReimbursementShortFall
//            accReimbursementMet.tintColor = UIColor.black
            
        }
    }
    
    private var isLoading: Bool = false {
        didSet {
            if isLoading {SVProgressHUD.show(withStatus: "Getting user info..")}
            else {SVProgressHUD.dismiss()}
        }
    }
    
    
    private var EXPANDED_CLIENT_INFO_HEIGHT: CGFloat = 180
    private var EXPANDED_COVERAGE_INFO_HEIGHT: CGFloat = 750
    private var EXPANDED_CHARTS_INFO_HEIGHT: CGFloat = 800
    
    
    var client: ClientModel! = nil
    var clientViewModel: ClientViewModel?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onUpdatePolicyDetails(_:)), name: .didUpdatePolicyDetails, object: nil)

        // Do any additional setup after loading the view.
    }
    
    @objc func onUpdatePolicyDetails(_ notification:Notification) {
        getBenefits()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getBenefits()
        configure()
    }
    
    func configure() {
        if client == nil {
            return
        }
        icLabel.text = client.nric
        dobLabel.text = client.dateOfBirth.toFormat("dd MMM YYYY")
        mobileNoLabel.text = client!.handphone
        AddressLabel.text = client!.address1
        annualIncomeText.text = clientViewModel?.annualIncomeSGD
        
        deathOptText.text = "(\(Agent.current.agentPreferences.deathOpt) times)"
        tpdOptText.text = "(\(Agent.current.agentPreferences.deathOpt) times)"
        critIllOptText.text = "(\(Agent.current.agentPreferences.critIllOpt) times)"
        earlyCrtiIllOptText.text = "(\(Agent.current.agentPreferences.critIllOpt) times)"
        accDeathOptText.text = ">S$ \(Agent.current.agentPreferences.accDeathOpt)"
        accTpdOptText.text = ">S$ \(Agent.current.agentPreferences.accDeathOpt)"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPremiiums" {
            let destination = segue.destination as! ChartsPremiumsViewController
            destination.client = client
        }
    }
    
    func getBenefits() {
        isLoading = true
        NetworkUtility.shared.HTTPGet(route: "\(Globals.Routes.baseClientRoute)\(client.id)/getBenefitSummary") { (response) in
            print(response)
            self.benefits = Benefits(benefits: response.arrayValue, annualIncome: self.clientViewModel?.annualIncomeValue ?? 0)
            self.isLoading = false
        }
    }
    
    //MARK: - Buttons
    
    
    @IBAction func collapseSection(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        switch sender.tag {
        case 0:
            if sender.isSelected {
                self.clientInfoHeightConstraint.constant = 60
            }
            else {
                self.clientInfoHeightConstraint.constant = self.EXPANDED_CLIENT_INFO_HEIGHT
            }
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        case 1:
            if sender.isSelected {
                coverageInfoHeightConstraint.constant = 60
            }
            else {
                coverageInfoHeightConstraint.constant = EXPANDED_COVERAGE_INFO_HEIGHT
            }
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        case 2:
            if sender.isSelected {
                chartsInfoheightConstraint.constant = 60
            }
            else {
                chartsInfoheightConstraint.constant = EXPANDED_CHARTS_INFO_HEIGHT
            }
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        default:
            return
        }
    }

}

extension ContactsInfoViewController: PoliciesListDelegate {
    func didSelectPolicyModel(policy: PolicyModel) {
        return
    }
    
    func refreshContactInfo() {
        self.getBenefits()
    }
    
    
}
