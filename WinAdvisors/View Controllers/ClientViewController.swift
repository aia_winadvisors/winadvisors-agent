//
//  ClientViewController.swift
//  
//
//  Created by Bilguun Batbold on 21/6/18.
//

import UIKit
import SwiftDate
import SVProgressHUD

class ClientViewController: UIViewController {
    
    @IBOutlet weak var clientNameLabel: UILabel!
    @IBOutlet weak var clientDescriptionLabel: UILabel!
    @IBOutlet weak var clientEditButton: UIButton!
    @IBOutlet weak var clientInfoLabel: UILabel!
    @IBOutlet weak var insurancePolicyLabel: UILabel!
    @IBOutlet weak var clientContainerView: UIView!
    @IBOutlet weak var policyContainerView: UIView!
    
    var client: ClientModel! = nil
    var clientViewModel: ClientViewModel?
    
    var selectedPolicy: PolicyModel?
    
    var isLoading: Bool = false {
        didSet {
            if isLoading {
                SVProgressHUD.show()
            }
            else {
                SVProgressHUD.dismiss()
            }
        }
    }
    

    override func viewDidLoad() {
        isLoading = true
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configure()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showContactInfo" {
            let dest = segue.destination as! ContactsInfoViewController
            dest.client = client!
            dest.clientViewModel = clientViewModel
        }
        
        if segue.identifier == "showPolicyInfo" {
            let dest = segue.destination as! ClientPoliciesViewController
            dest.client = client!
            dest.clientViewModel = clientViewModel
            dest.delegate = self
        }
        
        if segue.identifier == "addPolicy" {
            let dest = segue.destination as! NewPolicy2ViewController
            dest.client = client!
        }
        
        if segue.identifier == "editPolicy" {
            let dest = segue.destination as! NewPolicy2ViewController
            dest.client = client!
            dest.policy = selectedPolicy
        }
    }
    
    func configure() {
        if client != nil {
            clientNameLabel.text = clientViewModel?.name
            clientDescriptionLabel.text = clientViewModel?.ageInYears
            clientViewModel?.populateFamilyOf()
        }
        
        clientEditButton.imageView?.contentMode = .scaleAspectFit
        isLoading = false
    }
    
    
    //MARK: - Buttons
    
    
    @IBAction func editClientDidTap(_ sender: UIButton) {
        guard let storyboard = storyboard, let editClientVC = storyboard.instantiateViewController(withIdentifier: "newClientViewController") as? NewClientViewController else {return}
        editClientVC.client = client
        self.navigationController?.pushViewController(editClientVC, animated: true)
//        self.present(editClientVC, animated: true) {
//            editClientVC.client = self.client
//        }
    }
    
    @IBAction func generateSummaryDidTap(_ sender: UIButton) {
        let clientId = client.id
        let agentId = Agent.current.id
        let urlString = "\(Globals.shared.HOST_URL)/policySummaryPDF?agentId=\(agentId)&clientId=\(clientId)"
        guard let url = URL(string: urlString) else {return}
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func backButton(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tabSelected(_ sender: UIButton) {
        deselectTab()
        switch sender.tag {
        case 0:
            clientInfoLabel.alpha = 1
            clientContainerView.isHidden = false
        case 1:
            insurancePolicyLabel.alpha = 1
            policyContainerView.isHidden = false
        default:
            break
        }
    }
    
    func deselectTab() {
        clientInfoLabel.alpha = 0.4
        insurancePolicyLabel.alpha = 0.4
        clientContainerView.isHidden = true
        policyContainerView.isHidden = true
    }
}


extension ClientViewController: PoliciesListDelegate {
    func refreshContactInfo() {
        
    }
    
    func didSelectPolicyModel(policy: PolicyModel) {
        selectedPolicy = policy
        self.performSegue(withIdentifier: "editPolicy", sender: self)
    }
    
    
}
