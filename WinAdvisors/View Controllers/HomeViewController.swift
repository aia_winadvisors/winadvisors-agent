//
//  HomeViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 26/1/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import GoogleSignIn
import GoogleAPIClientForREST
import UIKit
import SideMenu
import JTAppleCalendar
import GoogleSignIn
import GoogleAPIClientForREST
import SwiftyJSON
import Alamofire
import MaterialComponents.MaterialTypography
import MaterialComponents.MaterialButtons
import MaterialComponents.MaterialButtons_ButtonThemer
import CoreData
import SwiftDate


class HomeViewController: UIViewController {
    
    //MARK: - Variables
    
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var appointmentsTableView: UITableView!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var month: UILabel!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var clientsButton: UIButton!
    @IBOutlet weak var activityTrackerButton: UIButton!
    @IBOutlet weak var personalAssistantButton: UIButton!
    @IBOutlet weak var leaderboardButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
  //  @IBOutlet weak var todayButton: MDCButton!
    
    
    let dateFormatter = DateFormatter()
    let outsideMonthColor = #colorLiteral(red: 0.9251488447, green: 0.9287230372, blue: 0.9766724706, alpha: 0.5)
    let monthColor = UIColor.black
    let selectedMonthColor = UIColor(hexString: "0x3a294b")
    let currentDateSelectedColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    private let calendarService = GTLRCalendarService()
    var eventsFromServer: [String:GTLRCalendar_Event] = [:]

    var calendarEvents = [CalendarEventModel]()
    
    //SETTINGS
    
    let SECTION_HEADER_HEIGHT: CGFloat = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Globals.shared.MainNavController = self.navigationController
        if GIDSignIn.sharedInstance().currentUser != nil {
            calendarService.authorizer = GIDSignIn.sharedInstance().currentUser.authentication.fetcherAuthorizer()
            fetchEvents()
        }
        
        navigationController?.navigationBar.prefersLargeTitles = true
        setup()
        setupSideButtons()
        Agent.current.updatePreferences()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        ShowContacts(self)
//       showTodayCalendar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setup() {

        self.navigationController?.isNavigationBarHidden = true
        self.appointmentsTableView.register(UINib.init(nibName: "CalendarEventCell", bundle: nil), forCellReuseIdentifier: "CalendarEventCell")
        setupCalendarView()
    }
    
    func setupSideButtons() {
        homeButton.imageView?.contentMode = .scaleAspectFit
        clientsButton.imageView?.contentMode = .scaleAspectFit
        activityTrackerButton.imageView?.contentMode = .scaleAspectFit
        personalAssistantButton.imageView?.contentMode = .scaleAspectFit
        leaderboardButton.imageView?.contentMode = .scaleAspectFit
        settingsButton.imageView?.contentMode = .scaleAspectFit
        logoutButton.imageView?.contentMode = .scaleAspectFit
    }
    
    func setupCalendarView() {
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
//        year.font = MDCTypography.display2Font()
//        year.alpha = MDCTypography.display2FontOpacity()
//        month.font = MDCTypography.display3Font()
        
//        let date = Date()
//        self.calendarView.selectDates([date])
//        self.calendarView.scrollToDate(date)
        

//        calendarView.scrollingMode = .stopAtEachSection
//        calendarView.scrollToDate(date, triggerScrollToDateDelegate: true, animateScroll: true, preferredScrollPosition: nil, extraAddedOffset: 0, completionHandler: {
//            self.calendarView.selectDates([date])
//            self.calendarView.scrollToDate(date)
//        })
    }

    //GOOGLE SIGN IN
    // Construct a query and get a list of upcoming events from the user calendar
    func fetchEvents() {
        let query = GTLRCalendarQuery_EventsList.query(withCalendarId: "primary")
        let endOfNextWeek = (Date() + 7.days).dateAt(.endOfWeek)
        query.timeMin = GTLRDateTime(date: Date())
        query.timeMax = GTLRDateTime(date: endOfNextWeek)
        query.singleEvents = true
        query.orderBy = kGTLRCalendarOrderByStartTime
        calendarService.executeQuery(
            query,
            delegate: self,
            didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:)))
    }
    
    // Display the start dates and event summaries in the UITextView
    @objc func displayResultWithTicket(
        ticket: GTLRServiceTicket,
        finishedWithObject response : GTLRCalendar_Events,
        error : NSError?) {
        
        if let error = error {
            print(error.localizedDescription)
            return
        }
        
        if let events = response.items, !events.isEmpty {
            for event in events {
                let startDate = DateInRegion.init(event.start?.dateTime?.stringValue ?? event.start?.date?.stringValue ?? Date().toString(), region: .local)
                let endDate = DateInRegion.init(event.start?.dateTime?.stringValue ?? event.end?.date?.stringValue ?? Date().toString(), region: .local)
                if startDate == nil  {
                    return
                }
                let startHour = startDate!.hour < 12 ? "\(startDate!.hour)AM" : "\(startDate!.hour-12)PM"
                var endHour = ""
                if endDate != nil {
                    endHour = endDate!.hour < 12 ? "\(endDate!.hour)AM" : "\(endDate!.hour-12)PM"
                }
                let calendarEvent = CalendarEventModel.init(eventName: event.summary ?? "", eventStartTime: startHour, eventEndTime: endHour, eventLocation: event.location == nil ? "" : event.location!, date: startDate!.toString(), month: startDate!.monthName(SymbolFormatStyle.default), monthDay:"\(startDate!.monthDays)", weekDay: startDate!.weekdayName(SymbolFormatStyle.short))
                calendarEvents.append(calendarEvent)
            }
        } else {
            print("No upcoming events found.")
        }
        self.calendarView.reloadData()
        self.appointmentsTableView.reloadData()
    }
    
    
    //MARK: - Buttons
    
    @IBAction func ShowSideMenu(_ sender: Any) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func showTodayCalendar() {
        let date = Date()
        calendarView.scrollToDate(date) {
            self.calendarView.selectDates([date])
        }
    }
    
    @IBAction func ShowContacts(_ sender: Any) {
        Navigate(storyBoardName: "Clients")
        Globals.shared.CurrentSideMenu = .Clients
    }
    
    @IBAction func ShowNotification(_ sender: Any) {
        Navigate(storyBoardName: "Notification")
        Globals.shared.CurrentSideMenu = .Notification
    }
    
    @IBAction func ShowSettings(_ sender: Any) {
        Navigate(storyBoardName: "Settings")
        Globals.shared.CurrentSideMenu = .Settings
    }
    
    func Navigate(storyBoardName:String) {
        let newVC = storyboard!.instantiateViewController(withIdentifier: storyBoardName)
        Globals.shared.MainNavController?.setViewControllers([newVC], animated: true)
        dismiss(animated: true, completion: nil)
    }
}

extension HomeViewController: JTAppleCalendarViewDataSource {
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        dateFormatter.dateFormat = "yyyy MM dd"
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Calendar.current.locale
        
        let startDate = dateFormatter.date(from: "2017 01 01")
        let endDate = dateFormatter.date(from: "2025 12 31")
        let parameters = ConfigurationParameters(startDate: startDate!, endDate: endDate!)
        return parameters
    }
}

extension HomeViewController:  JTAppleCalendarViewDelegate {
    //Display cell
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomViewCell", for: indexPath) as! CustomViewCell
        cell.isHidden = false
        cell.dataLabel.text = cellState.text
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        handleCellEvents(view: cell, cellState: cellState)
        
        return cell
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setUpViewsOfCalendar(from: visibleDates)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
    }
    
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
        guard let cell = view as? CustomViewCell else {return}
        
        if cellState.isSelected {
            cell.dataLabel.textColor = currentDateSelectedColor
        }
        else {
            if cellState.dateBelongsTo == .thisMonth {
                cell.dataLabel.textColor = monthColor
            } else {
                cell.dataLabel.textColor = outsideMonthColor
            }
        }
    }
    
    func handleCellEvents(view: JTAppleCell?, cellState: CellState) {
        guard let cell = view as? CustomViewCell else {return}
        
        cell.eventDotView.isHidden = !eventsFromServer.contains(where: { $0.key == DateFormatter.localizedString(from: cellState.date, dateStyle: .short, timeStyle: .none) })
    }
    
    func handleCellSelected(view: JTAppleCell?, cellState: CellState) {
        guard let cell = view as? CustomViewCell else {return}
        if cellState.isSelected {
            cell.selectedView.isHidden = false
        }
        else {
            cell.selectedView.isHidden = true
        }
    }
    
    func setUpViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first!.date
        dateFormatter.dateFormat = "yyyy"
        year.text = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "MMMM"
        month.text = dateFormatter.string(from: date)
    }
    
}

extension UIColor {
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = String(hexString[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}



extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
//        if tableView.tag == 0 {
//            return 2
//        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return calendarEvents.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarEventCell", for: indexPath) as! CalendarEventCell
            cell.selectionStyle = .none
            cell.configure(with: calendarEvents[indexPath.row])
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "caseCell", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if tableView.tag == 0 {
            view.tintColor = UIColor.white
            let header = view as! UITableViewHeaderFooterView
            header.textLabel?.textColor = UIColor.darkGray
            header.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView.tag == 0 {
            let title = "All upcoming appointments (2 weeks) :"
            
//            if section == 0 {
//                title = "This Week"
//            }
//            else {
//                title = "Next Week"
//            }
            
            return title
        }
        
        return ""
    }
}
