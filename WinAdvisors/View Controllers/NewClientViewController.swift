//
//  NewClientViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 30/10/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTypography
import DropDownMenuKit
import SwiftDate
import SVProgressHUD
import SwiftyJSON
import RSSelectionMenu

class NewClientViewController: UIViewController {
    
    var toolBar = UIToolbar()
    var datePicker  = UIDatePicker()
    
    @IBOutlet weak var nricName: MDCTextField!
    var nameController: MDCTextInputControllerFilled!
    @IBOutlet weak var prefName: MDCTextField!
    var prefNameController: MDCTextInputControllerFilled!
    @IBOutlet weak var nric: MDCTextField!
    var nricController: MDCTextInputControllerFilled!
    @IBOutlet weak var clientType: MDCTextField!
    var clientTypeController: MDCTextInputControllerFilled!
    @IBOutlet weak var email: MDCTextField!
    var emailController: MDCTextInputControllerFilled!
    @IBOutlet weak var dateOfBirth: MDCTextField!
    var dateOfBirthController: MDCTextInputControllerFilled!
    @IBOutlet weak var gender: MDCTextField!
    var genderController: MDCTextInputControllerFilled!
    @IBOutlet weak var race: MDCTextField!
    var raceController: MDCTextInputControllerFilled!
    @IBOutlet weak var familyOf: MDCTextField!
    var familyOfController: MDCTextInputControllerFilled!
    @IBOutlet weak var relationship: MDCTextField!
    var relationshipController: MDCTextInputControllerFilled!
    @IBOutlet weak var nationality: MDCTextField!
    var nationalityController: MDCTextInputControllerFilled!
    @IBOutlet weak var handphone: MDCTextField!
    var handphoneController: MDCTextInputControllerFilled!
    @IBOutlet weak var address1: MDCTextField!
    var address1Controller: MDCTextInputControllerFilled!
    @IBOutlet weak var address2: MDCTextField!
    var address2Controller: MDCTextInputControllerFilled!
    @IBOutlet weak var postalCode: MDCTextField!
    var postalCodeController: MDCTextInputControllerFilled!
    @IBOutlet weak var remark: MDCTextField!
    var remarkController: MDCTextInputControllerFilled!
    @IBOutlet weak var grossAnnualIncome: MDCTextField!
    var grossAnnualIncomeController: MDCTextInputControllerFilled!
    @IBOutlet weak var netAnnualIncome: MDCTextField!
    var netAnnualIncomeController: MDCTextInputControllerFilled!
    @IBOutlet weak var shortTermSavings: MDCTextField!
    var shortTermSavingsController: MDCTextInputControllerFilled!
    @IBOutlet weak var livingExpenses: MDCTextField!
    var livingExpensesController: MDCTextInputControllerFilled!
    @IBOutlet weak var nextContact: MDCTextField!
    var nextContactController: MDCTextInputControllerFilled!
    var allTextFieldControllers = [MDCTextInputControllerFloatingPlaceholder]()
    
    @IBOutlet weak var clientListTableView: UITableView!
    
    @IBOutlet weak var updateOrAddButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var client: ClientModel?
    var clientViewModel: ClientViewModel?
    
    var allClientList = [ClientModel]()
    
    private var familyOfClient: ClientModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
        guard let client = client else {return}
        populateValues(client: client)
        

        // Do any additional setup after loading the view.
    }
    
    func setupTextFields() {
        nameController = MDCTextInputControllerFilled(textInput: nricName)
        nricName.delegate = self
        nameController.placeholderText = "NRIC Name"
        allTextFieldControllers.append(nameController)
        
        prefNameController = MDCTextInputControllerFilled(textInput: prefName)
        prefName.delegate = self
        prefNameController.placeholderText = "Preferred Name"
        allTextFieldControllers.append(prefNameController)
        
        clientTypeController = MDCTextInputControllerFilled(textInput: clientType)
        clientType.delegate = self
        clientTypeController.placeholderText = "Client Type"
        clientType.text = "Prospect"
        allTextFieldControllers.append(clientTypeController)
        
        nricController = MDCTextInputControllerFilled(textInput: nric)
        nric.delegate = self
        nricController.placeholderText = "Client NRIC / Passport"
        allTextFieldControllers.append(nricController)
        
        emailController = MDCTextInputControllerFilled(textInput: email)
        email.delegate = self
        emailController.placeholderText = "Client Email"
        allTextFieldControllers.append(emailController)
        
        dateOfBirthController = MDCTextInputControllerFilled(textInput: dateOfBirth)
        dateOfBirth.delegate = self
        dateOfBirthController.placeholderText = "Client Date of Birth (YYYY-MM-dd)"
        dateOfBirth.text = DateInRegion().toFormat("YYYY-MM-dd")
        allTextFieldControllers.append(dateOfBirthController)
        
        genderController = MDCTextInputControllerFilled(textInput: gender)
        gender.delegate = self
        genderController.placeholderText = "Client Gender (Male / Female)"
        allTextFieldControllers.append(genderController)
        
        raceController = MDCTextInputControllerFilled(textInput: race)
        race.delegate = self
        raceController.placeholderText = "Client Race"
        allTextFieldControllers.append(raceController)
        
        familyOfController = MDCTextInputControllerFilled(textInput: familyOf)
        familyOf.delegate = self
        familyOfController.placeholderText = "Family Of (leave empty if main)"
        allTextFieldControllers.append(familyOfController)
        
        relationshipController = MDCTextInputControllerFilled(textInput: relationship)
        relationship.delegate = self
        relationshipController.placeholderText = "Relationship (leave empty if main)"
        allTextFieldControllers.append(relationshipController)
        
        nationalityController = MDCTextInputControllerFilled(textInput: nationality)
        nationality.delegate = self
        nationalityController.placeholderText = "Client Nationality"
        allTextFieldControllers.append(nationalityController)
        
        handphoneController = MDCTextInputControllerFilled(textInput: handphone)
        handphone.delegate = self
        handphone.keyboardType = .numberPad
        handphoneController.placeholderText = "Client Handphone"
        allTextFieldControllers.append(handphoneController)
        
        address1Controller = MDCTextInputControllerFilled(textInput: address1)
        address1.delegate = self
        address1Controller.placeholderText = "Client Address 1"
        allTextFieldControllers.append(address1Controller)
        
        address2Controller = MDCTextInputControllerFilled(textInput: address2)
        address2.delegate = self
        address2Controller.placeholderText = "Client Address 2"
        allTextFieldControllers.append(address2Controller)
        
        postalCodeController = MDCTextInputControllerFilled(textInput: postalCode)
        postalCode.delegate = self
        postalCodeController.placeholderText = "Client Postal Code"
        allTextFieldControllers.append(postalCodeController)
        
        remarkController = MDCTextInputControllerFilled(textInput: remark)
        remark.delegate = self
        remarkController.placeholderText = "Remarks (if any)"
        allTextFieldControllers.append(remarkController)
        
        shortTermSavingsController = MDCTextInputControllerFilled(textInput: shortTermSavings)
        shortTermSavings.delegate = self
        shortTermSavings.keyboardType = .decimalPad
        shortTermSavings.text = "0"
        shortTermSavingsController.placeholderText = "Short Term Savings (Annually S$)"
        allTextFieldControllers.append(shortTermSavingsController)
        
        livingExpensesController = MDCTextInputControllerFilled(textInput: livingExpenses)
        livingExpenses.delegate = self
        livingExpenses.keyboardType = .decimalPad
        livingExpensesController.placeholderText = "Living Expenses (Annually S$)"
        livingExpenses.text = "0"
        allTextFieldControllers.append(livingExpensesController)
        
        grossAnnualIncomeController = MDCTextInputControllerFilled(textInput: grossAnnualIncome)
        grossAnnualIncome.delegate = self
        grossAnnualIncome.keyboardType = .decimalPad
        grossAnnualIncomeController.placeholderText = "Gross Annual Income (S$)"
        grossAnnualIncome.text = "0"
        allTextFieldControllers.append(grossAnnualIncomeController)
        
        netAnnualIncomeController = MDCTextInputControllerFilled(textInput: netAnnualIncome)
        netAnnualIncome.delegate = self
        grossAnnualIncome.keyboardType = .decimalPad
        netAnnualIncomeController.placeholderText = "Net Annual Income (S$)"
        netAnnualIncome.text = "0"
        allTextFieldControllers.append(netAnnualIncomeController)
        
        nextContactController = MDCTextInputControllerFilled(textInput: nextContact)
        nextContact.delegate = self
        nextContact.text = DateInRegion().toFormat("YYYY-MM-dd")
        nextContactController.placeholderText = "Next Contact Date (YYYY-MM-dd)"
        allTextFieldControllers.append(nextContactController)

        registerKeyboardNotifications()
    }
    
    func populateValues(client: ClientModel) {
        self.navigationItem.title = "Update Client"
        clientViewModel = ClientViewModel.init(client: client)
        nricName.text = clientViewModel?.name
        clientType.text = clientViewModel?.clientType
        nric.text = clientViewModel?.nric
        prefName.text = clientViewModel?.prefName
        email.text = clientViewModel?.email
        dateOfBirth.text = clientViewModel?.dateOfBirth
        gender.text = clientViewModel?.gender
        race.text = clientViewModel?.race
        nationality.text = clientViewModel?.nationality
        handphone.text = clientViewModel?.handphone
        address1.text = clientViewModel?.address1
        address2.text = clientViewModel?.address2
        postalCode.text = clientViewModel?.postalCode
        familyOf.text = clientViewModel?.familyOfName
        relationship.text = clientViewModel?.relationship
        shortTermSavings.text = clientViewModel?.shortTermSavings
        livingExpenses.text = clientViewModel?.livingExpenses
        remark.text = clientViewModel?.remark
        nextContact.text = clientViewModel?.nextContact
        updateOrAddButton.setTitle(("Update Client"), for: .normal)
        guard let grossIncome = clientViewModel?.annualIncomeValue, let netIncome = clientViewModel?.netAnnualIncomeValue else {return}
        grossAnnualIncome.text = String.init(format: "%.2f", grossIncome)
        netAnnualIncome.text = String.init(format: "%.2f", netIncome)
    }
    
    //MARK: - BUTTONS
    
    private var textFieldToUpdate: MDCTextField?
    
    @IBAction func datePickerButtonClicked(_ sender: UIButton) {
        datePicker = UIDatePicker.init()
        datePicker.backgroundColor = UIColor.white
        datePicker.autoresizingMask = .flexibleWidth
        datePicker.datePickerMode = .date
        
        switch sender.tag {
        case 0:
            textFieldToUpdate = dateOfBirth
        case 1:
            textFieldToUpdate = nextContact
        default:
            textFieldToUpdate = dateOfBirth
        }
        
        datePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
        datePicker.frame = CGRect(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(datePicker)
        
        toolBar = UIToolbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.onDoneButtonClick))]
        toolBar.sizeToFit()
        self.view.addSubview(toolBar)
    }
    
    @objc func dateChanged(_ sender: UIDatePicker?) {
        guard let date = sender?.date, let textField = textFieldToUpdate else {return}
        textField.text = date.toFormat("YYYY-MM-dd")
    }
    
    @objc func onDoneButtonClick() {
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
    }
    
    @IBAction func updateOrAdd(_ sender: Any) {
        if !validateValues() {
            return
        }
        
        if client != nil {
            updateClient()
        }
        else {
            addNewClient()
        }
    }
    
    @IBAction func genderSelectionDidTap(_ sender: Any) {
        let simpleDataArray = ["MALE", "FEMALE"]
        let simpleSelectedArray = [String]()
        
        
        let selectionMenu =  RSSelectionMenu(dataSource: simpleDataArray) { (cell, object, indexPath) in
            cell.textLabel?.text = object
            
            // Change tint color (if needed)
            cell.tintColor = .orange
        }
        
        // set default selected items when menu present on screen.
        // Here you'll get onDidSelectRow
        
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems)  in
            
            // update your existing array with updated selected items, so when menu presents second time updated items will be default selected.
            self?.gender.text = item
            
        }
        
        
        // show as PresentationStyle = Push
        selectionMenu.show(style: .push, from: self)
    }
    
    private func addNewClient() {
        SVProgressHUD.show()
        var parameters: [String:String] = ["name":nricName.text!.capitalized, "prefName":prefName.text!.capitalized, "clientType":clientType.text!, "nric":nric.text!, "email":email.text!, "dateOfBirth":dateOfBirth.text!, "gender":gender.text!, "race":race.text!, "familyOf": familyOfClient?.id ?? "", "relationship": relationship.text!, "nationality":nationality.text!, "remark":remark.text!, "handphone":handphone.text!, "address1":address1.text!, "address2":address2.text!, "postalCode":postalCode.text!, "shortTermSavings":shortTermSavings.text!, "livingExpenses":livingExpenses.text!, "nextContact":nextContact.text!, "annualIncome":grossAnnualIncome.text!, "netAnnualIncome":netAnnualIncome.text!]
        Agent.current.addNewClient(parameters: &parameters) { (response) in
            if response["_id"].exists() {
                self.navigationController?.popViewController(animated: true)
            }
            else {
                Globals.shared.showDefaultAlert(title: "Error", message: "Unknown error has occured")
            }
            SVProgressHUD.dismiss()
        }
    }
    
    private func updateClient() {
        clientViewModel?.name = nricName.text!
        clientViewModel?.clientType = clientType.text!
        clientViewModel?.nric = nric.text!
        clientViewModel?.prefName = prefName.text!
        clientViewModel?.email = email.text!
        clientViewModel?.dateOfBirth = dateOfBirth.text!
        clientViewModel?.gender = gender.text!
        clientViewModel?.race = race.text!
        clientViewModel?.familyOf = familyOfClient?.id ?? ""
        clientViewModel?.relationship = relationship.text!
        clientViewModel?.nationality = nationality.text!
        clientViewModel?.remark = remark.text!
        clientViewModel?.handphone = handphone.text!
        clientViewModel?.address1 = address1.text!
        clientViewModel?.address2 = address2.text!
        clientViewModel?.postalCode = postalCode.text!
        clientViewModel?.shortTermSavings = shortTermSavings.text!
        clientViewModel?.livingExpenses = livingExpenses.text!
        clientViewModel?.nextContact = nextContact.text!
        if let incomeString = (grossAnnualIncome.text), let income = Double(incomeString) {
            clientViewModel?.annualIncomeValue = income
        }
        if let netIncomeString = (netAnnualIncome.text), let income = Double(netIncomeString) {
            clientViewModel?.netAnnualIncomeValue = income
        }
        SVProgressHUD.show(withStatus: "Updating Client")
        clientViewModel?.updateClient(completion: { (response) in
            if response["result"].intValue == 0 {
                self.navigationController?.popViewController(animated: true)
            }
            else {
                Globals.shared.showDefaultAlert(title: "Error", message: "Unknown error has occured")
            }
            SVProgressHUD.dismiss()
        })
    }
    
    private func validateValues() -> Bool {
        for controller in allTextFieldControllers {
            if controller.placeholderText!.lowercased().contains("family")  || controller.placeholderText!.lowercased().contains("remarks") || controller.placeholderText!.lowercased().contains("relationship") || controller.placeholderText!.lowercased().contains("next contact")  {
                continue
            }
            if controller.textInput?.text == "" {
                controller.setErrorText("Field should not be empty", errorAccessibilityValue: nil)
                return false
            }
        }
        return true
    }
}

extension NewClientViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allClientList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = allClientList[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.familyOfClient = allClientList[indexPath.row]
        familyOf.text = familyOfClient?.name
        address1.text = familyOfClient?.address1
        address2.text = familyOfClient?.address2
        postalCode.text = familyOfClient?.postalCode
        clientListTableView.isHidden = true
    }
}


extension NewClientViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clientListTableView.isHidden = true
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        
        guard let rawText = textField.text else {
            return true
        }
        
        let fullString = NSString(string: rawText).replacingCharacters(in: range, with: string)

        if textField == dateOfBirth {
            guard let _ = fullString.toDate("yyyy-MM-dd") else {
                dateOfBirthController.setErrorText("Error: Please enter valid date",
                                                   errorAccessibilityValue: nil)
                return true
            }
            dateOfBirthController.setErrorText(nil, errorAccessibilityValue: nil)
        }
        
        if textField == familyOf {
            allClientList = Agent.current.searchClientNames(text: fullString)
            clientListTableView.isHidden = false
            clientListTableView.reloadData()
            return true
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let index = textField.tag
        if index + 1 < allTextFieldControllers.count,
            let nextField = allTextFieldControllers[index + 1].textInput {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return false
    }
}

// MARK: - Keyboard Handling
extension NewClientViewController {
    func registerKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow(notif:)),
            name: UIResponder.keyboardWillChangeFrameNotification,
            object: nil)
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow(notif:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillHide(notif:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }
    
    @objc func keyboardWillShow(notif: Notification) {
        guard let frame = notif.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
        scrollView.contentInset = UIEdgeInsets(top: 0.0,
                                               left: 0.0,
                                               bottom: frame.height,
                                               right: 0.0)
    }
    
    @objc func keyboardWillHide(notif: Notification) {
        scrollView.contentInset = UIEdgeInsets()
    }
}
