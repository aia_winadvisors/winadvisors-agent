//
//  ClientPolicyInfoViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 28/10/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class ClientPolicyInfoViewController: UIViewController {
    
    @IBOutlet weak var policyHolderNameLabel: UILabel!
    @IBOutlet weak var policyInsuredNameLabel: UILabel!
    @IBOutlet weak var benefitsLabel: UILabel!
    @IBOutlet weak var purchaseDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var totalPremiumLabel: UILabel!

    var policyInfo: JSON = []
    var policy: PolicyModel! = nil
    var client: ClientModel! = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()

        // Do any additional setup after loading the view.
    }
    
    func setup() {
        if client == nil {
            return
        }
        policyHolderNameLabel.text = client.name
        policyInsuredNameLabel.text = client.name
        if policy == nil {
            return
        }
        purchaseDateLabel.text = policy.policyStartDate.toFormat(("yyyy-MM-dd"))
        endDateLabel.text = policy.policyEndDate.toFormat(("yyyy-MM-dd"))
        totalPremiumLabel.text = "S$\(policy.annualPremium)"
    }
    
    @IBAction func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
//
//    func getBenefits() {
//        NetworkUtility.shared.HTTPGet(route: "/api/policy/\(client.id)/getBenefits") { (response) in
//            self.benefits = Benefits(benefits: response, annualIncome: self.client.annualIncome != nil ? self.client.annualIncome! : 0)
//        }
//    }
//
}
