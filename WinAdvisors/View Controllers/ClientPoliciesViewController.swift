//
//  ClientPoliciesViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 27/10/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit
import SwiftyJSON
import NotificationCenter

class PolicyCell: UITableViewCell {
    @IBOutlet weak var policyName: UILabel!
    @IBOutlet weak var premium: UILabel!
    @IBOutlet weak var purchaseDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
}

class ClientPoliciesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let ROW_HEIGHT: CGFloat = 80
    var client: ClientModel! = nil
    var policiesList: JSON = []
    var selectedPolicy: PolicyModel! = nil
    var clientViewModel: ClientViewModel?
    var policyViewModel: PolicyViewModel?
    var listOfPolicies = [PolicyModel]()
    
    var delegate: PoliciesListDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        listOfPolicies.removeAll()
        client.getPolicies { (policies) in
            self.listOfPolicies = policies
            print(self.listOfPolicies)
            self.tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "policyInfo" {
            let dest = segue.destination as! ClientPolicyInfoViewController
            dest.client = client
            dest.policy = selectedPolicy
        }
    }
    

}

extension ClientPoliciesViewController: UITableViewDelegate, UITableViewDataSource {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.listOfPolicies.count == 0 {
            self.tableView.setEmptyMessage("Client have not bought any policies yet.")
        }
        else {
            self.tableView.restore()
        }
        return self.listOfPolicies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PolicyCell
        let policy = self.listOfPolicies[indexPath.row]
        cell.policyName.text = policy.name
        cell.premium.text = "S$\(policy.annualPremium)"
        cell.purchaseDate.text = policy.policyStartDate.toFormat(("yyyy-MM-dd"))
        cell.endDate.text = policy.policyEndDate.toFormat(("yyyy-MM-dd"))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ROW_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            let policy = self.listOfPolicies[indexPath.row]
            self.listOfPolicies.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.deletePolicy(policyId: policy.id)
            print(self.policiesList)
        }
        
        return [delete]
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > listOfPolicies.count - 1 {
            return
        }
        selectedPolicy = listOfPolicies[indexPath.row]
//        let vc = storyboard?.instantiateViewController(withIdentifier: "policyInfo") as! ClientPolicyInfoViewController
//        vc.client = client
//        vc.policy = selectedPolicy
//        vc.transitioningDelegate = self
//        vc.modalPresentationStyle = .custom
//        self.present(vc, animated: true, completion: nil)
        
        delegate?.didSelectPolicyModel(policy: selectedPolicy)
    }
    
    func deletePolicy(policyId: String) {
        let route = Globals.Routes.basePolicyRoute + "\(policyId)"
        NetworkUtility.shared.HTTPDelete(route: route) { (response) in
            Globals.shared.showDefaultAlert(title: "Success", message: "Policy with id \(policyId) has been deleted.")
            NotificationCenter.default.post(name: .didUpdatePolicyDetails, object: nil)
        }
    }
}


extension ClientPoliciesViewController: UIViewControllerTransitioningDelegate {
    
    func presentationController(forPresented presented: UIViewController,
                                presenting: UIViewController?,
                                source: UIViewController) -> UIPresentationController? {
        let presentationController = BlurredPresentationController(presentedViewController: presented,
                                                                          presenting: presenting)
        return presentationController
    }
}



protocol PoliciesListDelegate {
    func didSelectPolicyModel(policy: PolicyModel)
    func refreshContactInfo()
}
