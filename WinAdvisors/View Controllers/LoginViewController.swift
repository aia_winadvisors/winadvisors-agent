//
//  ViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 26/1/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields
import GoogleAPIClientForREST
import GoogleSignIn
import SwiftyJSON
import SVProgressHUD

class LoginViewController: UIViewController, UITextFieldDelegate, GIDSignInDelegate, GIDSignInUIDelegate {

    //MARK: - Controls
    
    @IBOutlet weak var winLogo: UIImageView!
    @IBOutlet weak var signInButton: GIDSignInButton!
    
    // If modifying these scopes, delete your previously saved credentials by
    // resetting the iOS simulator or uninstall the app.
    private let scopes = [kGTLRAuthScopeCalendarReadonly]
    
    private let service = GTLRCalendarService()
    let output = UITextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure Google Sign-in.
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
        signInButton.style = .wide
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        SVProgressHUD.show()
        if let error = error {
            print(error.localizedDescription)
            self.service.authorizer = nil
        } else {
            guard let idToken = user.authentication.idToken else { return }
            Agent.current.login(name: user.profile.name, email: user.profile.email, idToken: idToken) { (response) in
                if response != JSON.null {
                    self.service.authorizer = user.authentication.fetcherAuthorizer()
                    self.performSegue(withIdentifier: "Login", sender: self)
                }
                else {
                    Globals.shared.showDefaultAlert(title: "Sign in error", message: "Please check with IT")
                }
                 SVProgressHUD.dismiss()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //called when logout is pressed from side bar
    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {
        GIDSignIn.sharedInstance().signOut()
    }


}

