//
//  NewPolicy2ViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 29/10/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit
import DropDownMenuKit
import MaterialComponents
import SwiftDate
import RSSelectionMenu
import SVProgressHUD
import SwiftyJSON

class NewPolicy2ViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var issuerDropDown: DropDownMenu!
    @IBOutlet weak var issuerDropDownViewFrame: UIView!
    @IBOutlet weak var issuerShowButton: MDCButton!
    @IBOutlet weak var issuerImageView: UIImageView!
    
    //textfields
    
    @IBOutlet weak var policyStartDate: UITextField!
    @IBOutlet weak var policyName: UITextField!
    @IBOutlet weak var policyNo: UITextField!
    @IBOutlet weak var clientAge: UITextField!
    @IBOutlet weak var policyEndAge: UITextField!
    @IBOutlet weak var policyDuration: UITextField!
    @IBOutlet weak var premium: UITextField!
    @IBOutlet weak var premiumEndAge: UITextField!
    @IBOutlet weak var paymentFrequency: UITextField!
    @IBOutlet weak var paymentMode: UITextField!
    @IBOutlet weak var benefits: UITextField!
    @IBOutlet weak var remarks: UITextField!
    @IBOutlet weak var policyTypeButton: UIButton!
    @IBOutlet weak var cashValueAge: UITextField!
    @IBOutlet weak var cashValueAmount: UITextField!
    @IBOutlet weak var couponStartDate: UITextField!
    @IBOutlet weak var couponEndDate: UITextField!
    @IBOutlet weak var couponAmount: UITextField!
    
    
    @IBOutlet weak var addEditPolicyButton: UIButton!
    
    var selectedBenefits = [String]()
    var policyDurationYears = 0
    var policyBenefits = [[String:String]]()
    var benefitsJSON: JSON = []
    
    var client: ClientModel! = nil
    
    var policy: PolicyModel?
    var policyViewModel: PolicyViewModel?
    
    enum PolicyType: Int {
        case risk = 0, wealth_accum, wealth_mgmt
        
        var description: String {
            get {
                switch self {
                case .risk:
                    return "risk"
                case .wealth_mgmt:
                    return "wealth_mgmt"
                case .wealth_accum:
                    return "wealth_accum"
                }
            }
        }
    }
    
    
    private var selectedPaymentFrequency = PaymentFrequency.monthly
    enum PaymentFrequency: Int {
        case monthly = 0, quarterly, half_yearly, annually
        
        var description: String {
            get {
                switch self {
                case .monthly:
                    return "MONTHLY"
                case .quarterly:
                    return "QUARTERLY"
                case .half_yearly:
                    return "HALF-YEARLY"
                case .annually:
                    return "ANNUALLY"
                }
            }
        }
        
        var paymentMultiplicationValue: Int {
            get {
                switch self {
                case .monthly:
                    return 12
                case .quarterly:
                    return 4
                case .half_yearly:
                    return 2
                case .annually:
                    return 1
                }
            }
        }
    }
    
    private var policyType = PolicyType.risk
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerKeyboardNotifications()
        setupDropDowns()
        setupTextFields()
        
        if policy != nil {
            self.navigationItem.title = "Update Policy"
            policyViewModel = PolicyViewModel(policy: policy!)
            addEditPolicyButton.setTitle("Update Policy", for: .normal)
            setPolicyInfo()
        }
        // Do any additional setup after loading the view.
    }
    
    func setPolicyInfo() {
        policyName.text = policyViewModel?.name
        policyNo.text = policyViewModel?.policyNumber
        policyTypeButton.setTitle(policyViewModel?.policyType, for: .normal)
        policyStartDate.text = policyViewModel?.policyStartDate
        policyEndAge.text = policyViewModel?.policyEndAge
        policyDuration.text = policyViewModel?.policyDuration
        paymentMode.text = policyViewModel?.paymentMode
        policyBenefits = policyViewModel?.benefits ?? [[String:String]]()
        premiumEndAge.text = policyViewModel?.premiumEndAge
        premium.text = policyViewModel?.premium
        paymentFrequency.text = policyViewModel?.paymentFrequency.uppercased()
        remarks.text = policyViewModel?.remark
        cashValueAge.text = policyViewModel?.cashValueAge
        cashValueAmount.text = policyViewModel?.cashValueAmount
        couponStartDate.text = policyViewModel?.couponStartDate
        couponEndDate.text = policyViewModel?.couponEndDate
        couponAmount.text = policyViewModel?.couponAmount
        var benefitsString: String = ""
        for benefits in policyBenefits {
            let benefitName: String = benefits["name"] ?? ""
            let benefitValue: String = benefits["value"] ?? ""
            benefitsString.append(benefitName + ": " + benefitValue + " ")
        }
        self.benefits.text = benefitsString
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showBenefits" {
            let benefitsVC = segue.destination as! BenefitsViewController
            benefitsVC.benefits = self.policyBenefits
            benefitsVC.callback = { [weak self] callbackBenefits, benefitsDict in
                print(callbackBenefits)
                self?.benefitsJSON = callbackBenefits
                self?.policyBenefits = benefitsDict
                var benefitsString: String = ""
                for (_,subjson) in callbackBenefits {
                    print(subjson)
                    benefitsString.append(subjson["name"].stringValue + ": " + subjson["value"].stringValue + " ")
                }
                //                self?.policyBenefits = callbackBenefits
                self?.benefits.text = benefitsString
            }
        }
    }
    
    func setupTextFields() {
        policyStartDate.text = Date().toFormat("yyyy-MM-dd")
        clientAge.text = String.init(describing: client.age)
        clientAge.isEnabled = false
    }
    
    //MARK: - Buttons
    
    
    
    @IBAction func showIssuerDropDownButtonTap(_ sender: Any) {
        
        if issuerDropDown.isHidden {
            issuerDropDown.show()
            issuerDropDownViewFrame.isHidden = false
        }
        else {
            issuerDropDown.hide()
            issuerDropDownViewFrame.isHidden = true
        }
    }
    
    @IBAction func paymentFrequencyDidTap(_ sender: Any) {
        let simpleDataArray = [PaymentFrequency.monthly.description, PaymentFrequency.quarterly.description, PaymentFrequency.half_yearly.description, PaymentFrequency.annually.description]
        let simpleSelectedArray = [String]()
        
        // Show menu with datasource array - Default SelectionType = Single
        // Here you'll get cell configuration where you can set any text based on condition
        // Cell configuration following parameters.
        // 1. UITableViewCell   2. Object of type T   3. IndexPath
        
        let selectionMenu =  RSSelectionMenu(dataSource: simpleDataArray) { (cell, object, indexPath) in
            cell.textLabel?.text = object
            
            // Change tint color (if needed)
            cell.tintColor = .orange
        }
        
        // set default selected items when menu present on screen.
        // Here you'll get onDidSelectRow
        
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems)  in
            
            // update your existing array with updated selected items, so when menu presents second time updated items will be default selected.
            self?.paymentFrequency.text = item
            self?.selectedPaymentFrequency = PaymentFrequency.init(rawValue: index) ?? PaymentFrequency.monthly
        }
        
        
        // show as PresentationStyle = Push
        selectionMenu.show(style: .push, from: self)
    }
    
    @IBAction func policyTypeDidTap(_ sender: UIButton) {
        let simpleDataArray = ["RISK MANAGEMENT", "WEALTH ACCUMULATION", "WEALTH PRESERVATION (LEGACY)"]
        let simpleSelectedArray = [String]()
        
        
        let selectionMenu =  RSSelectionMenu(dataSource: simpleDataArray) { (cell, object, indexPath) in
            cell.textLabel?.text = object
        }
        
        // set default selected items when menu present on screen.
        // Here you'll get onDidSelectRow
        
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { (item, index, isSelected, selectedItems)  in
            sender.setTitle(selectedItems.first, for: .normal)
            self.policyType = PolicyType.init(rawValue: index) ?? PolicyType.risk
        }
            
        selectionMenu.show(style: .push, from: self)
    
    }
    
    @IBAction func paymentModeDidTap(_ sender: Any) {
        let simpleDataArray = ["CASH", "CREDIT CARD", "GIRO", "CPF-OA", "CPF-SA", "CPF-MA"]
        let simpleSelectedArray = [String]()
        
        // Show menu with datasource array - Default SelectionType = Single
        // Here you'll get cell configuration where you can set any text based on condition
        // Cell configuration following parameters.
        // 1. UITableViewCell   2. Object of type T   3. IndexPath
        
        let selectionMenu =  RSSelectionMenu(dataSource: simpleDataArray) { (cell, object, indexPath) in
            cell.textLabel?.text = object
            
            // Change tint color (if needed)
            cell.tintColor = .orange
        }
        
        // set default selected items when menu present on screen.
        // Here you'll get onDidSelectRow
        
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
            
            // update your existing array with updated selected items, so when menu presents second time updated items will be default selected.
            self?.paymentMode.text = item
        }
        
        selectionMenu.show(style: .push, from: self)
    }
    
    @IBAction func addEditPolicyDidTap(_ sender: Any) {
        if policy == nil {
            addNewPolicy()
        }
        else {
            editPolicy()
        }
    }
    
    @IBAction func cancelDidTap(_ sender: Any) {
        let alert = UIAlertController(title: "Clear all values?", message: "Doing so will clear all the populated values", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
             self.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
       
    }
    
    private func addNewPolicy() {
        SVProgressHUD.show(withStatus: "Adding New Policy")
        guard let name = policyName.text, let issuerName = issuerShowButton.titleLabel?.text , let policyNumber = policyNo.text,
            let startDate = policyStartDate.text, let premiumString = premium.text, var premium = Double(premiumString), let frequency = paymentFrequency.text?.lowercased(), let remark = remarks.text, let endDate = (startDate.toDate()?.dateByAdding(policyDurationYears, .year).toFormat("yyyy-MM-dd")), let policyEndAgeString = policyEndAge.text, let policyDuration = policyDuration.text, let paymentMode = paymentMode.text, let policyEndAgeYear = Double(policyEndAgeString), let premiumEndAge = premiumEndAge.text, let cashValueAge = cashValueAge.text, let cashValueAmount = cashValueAmount.text, let couponStartDate = couponStartDate.text, let couponEndDate = couponEndDate.text, let couponAmount = couponAmount.text else {return}
        
        let policyStartAgeYear = (startDate.toDate()?.year)! - client.dateOfBirth.year
        
        premium = premium * Double(selectedPaymentFrequency.paymentMultiplicationValue)
        
        print(policyBenefits)
        
        let parameters = ["name" : name, "issuerName" : issuerName, "policyNumber" : policyNumber, "policyStartDate" : startDate, "policyEndDate" : endDate, "policyStartAge": policyStartAgeYear, "policyEndAge" : policyEndAgeYear, "policyDuration":policyDuration, "paymentMode": paymentMode, "annualPremium" : premium, "paymentFrequency" : frequency, "policyType" : policyType.description, "benefits" : policyBenefits, "premiumEndAge":premiumEndAge, "remarks" : remark, "client" : client.id, "cashValueAge":cashValueAge, "cashValueAmount":cashValueAmount, "couponStartDate":couponStartDate, "couponEndDate":couponEndDate, "couponAmount":couponAmount] as [String : Any]
        
        NetworkUtility.shared.HTTPPost(route: Globals.Routes.basePoliciesRoute, parameters: parameters) { (response) in
            if response["Error"].exists() { //didn't get proper JSON response
                SVProgressHUD.dismiss()
                Globals.shared.showDefaultAlert(title: "Error", message: "Please contact the admin")
                print(response)
            }
            else {
                if response["_id"].stringValue != "" {
                    print(response)
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    Globals.shared.showDefaultAlert(title: "Error", message: "Please check your credentials.")
                }
                SVProgressHUD.dismiss()
            }
            SVProgressHUD.dismiss()
        }
    }
    private func editPolicy() {
        SVProgressHUD.show(withStatus: "Updating Policy")
        guard let name = policyName.text, let issuerName = issuerShowButton.titleLabel?.text , let policyNumber = policyNo.text,
            let startDate = policyStartDate.text, let premiumString = premium.text, var premium = Double(premiumString), let frequency = paymentFrequency.text?.lowercased(), let remark = remarks.text, let endDate = (startDate.toDate()?.dateByAdding(policyDurationYears, .year).toFormat("yyyy-MM-dd")), let policyEndAgeString = policyEndAge.text, let policyDuration = policyDuration.text, let paymentMode = paymentMode.text, let policyEndAgeYear = Double(policyEndAgeString), let premiumEndAge = premiumEndAge.text, let cashValueAge = cashValueAge.text, let cashValueAmount = cashValueAmount.text, let couponStartDate = couponStartDate.text, let couponEndDate = couponEndDate.text, let couponAmount = couponAmount.text else {return}
        
        let policyStartAgeYear = (startDate.toDate()?.year)! - client.dateOfBirth.year
        
        if frequency == "monthly" {
            premium *= 12
        }
        
        print(policyBenefits)
        
        let parameters = ["_id": policyViewModel?.id ?? "1", "name" : name, "issuerName" : issuerName, "policyNumber" : policyNumber, "policyStartDate" : startDate, "policyEndDate" : endDate, "policyStartAge": policyStartAgeYear, "policyEndAge" : policyEndAgeYear, "policyDuration":policyDuration, "paymentMode": paymentMode, "annualPremium" : premium, "paymentFrequency" : frequency, "policyType" : policyType.description, "benefits" : policyBenefits, "premiumEndAge":premiumEndAge, "remarks" : remark, "client" : client.id, "cashValueAge":cashValueAge, "cashValueAmount":cashValueAmount, "couponStartDate":couponStartDate, "couponEndDate":couponEndDate, "couponAmount":couponAmount] as [String : Any]
        
        
        NetworkUtility.shared.HTTPPut(route: Globals.Routes.basePoliciesRoute, parameters: parameters) { (response) in
            if response["Error"].exists() { //didn't get proper JSON response
                SVProgressHUD.dismiss()
                Globals.shared.showDefaultAlert(title: "Error", message: "Please contact the admin")
                print(response)
            }
            else {
                if response["_id"].stringValue != "" {
                    print(response)
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    Globals.shared.showDefaultAlert(title: "Error", message: "Please check your credentials.")
                }
                SVProgressHUD.dismiss()
            }
            SVProgressHUD.dismiss()
        }
    }
}

// MARK: - Keyboard Handling
extension NewPolicy2ViewController {
    func registerKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow(notif:)),
            name: UIResponder.keyboardWillChangeFrameNotification,
            object: nil)
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow(notif:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillHide(notif:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }
    
    @objc func keyboardWillShow(notif: Notification) {
        guard let frame = notif.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
        scrollView.contentInset = UIEdgeInsets(top: 0.0,
                                               left: 0.0,
                                               bottom: frame.height,
                                               right: 0.0)
    }
    
    @objc func keyboardWillHide(notif: Notification) {
        scrollView.contentInset = UIEdgeInsets()
    }
}

extension NewPolicy2ViewController: DropDownMenuDelegate {
    
    func didTapInDropDownMenuBackground(_ menu: DropDownMenu) {
        issuerDropDown.hide()
    }
    
    
    func setupDropDowns() {
        
        issuerDropDown = DropDownMenu(frame: issuerDropDownViewFrame.bounds)
        issuerDropDown.delegate = self
        
        let aia = DropDownMenuCell()
        aia.textLabel!.text = "AIA"
        aia.imageView!.image = #imageLiteral(resourceName: "issuer_logo_aia")
        aia.showsCheckmark = false
        aia.menuTarget = self
        aia.menuAction = #selector(NewPolicy2ViewController.issuerDropDownChosen(_:))
        let aviva = DropDownMenuCell()
        aviva.textLabel!.text = "AVIVA"
        aviva.imageView!.image = #imageLiteral(resourceName: "issuer_logo_aviva")
        aviva.showsCheckmark = false
        aviva.menuTarget = self
        aviva.menuAction = #selector(NewPolicy2ViewController.issuerDropDownChosen(_:))
        let axa = DropDownMenuCell()
        axa.textLabel!.text = "AXA"
        axa.imageView!.image = #imageLiteral(resourceName: "issuer_logo_axa")
        axa.showsCheckmark = false
        axa.menuTarget = self
        axa.menuAction = #selector(NewPolicy2ViewController.issuerDropDownChosen(_:))
        let greatEastern = DropDownMenuCell()
        greatEastern.textLabel!.text = "GREAT EASTERN"
        greatEastern.imageView!.image = #imageLiteral(resourceName: "issuer_logo_great_eastern")
        greatEastern.showsCheckmark = false
        greatEastern.menuTarget = self
        greatEastern.menuAction = #selector(NewPolicy2ViewController.issuerDropDownChosen(_:))
        let manulife = DropDownMenuCell()
        manulife.textLabel!.text = "MANULIFE"
        manulife.imageView!.image = #imageLiteral(resourceName: "issuer_logo_manulife")
        manulife.showsCheckmark = false
        manulife.menuTarget = self
        manulife.menuAction = #selector(NewPolicy2ViewController.issuerDropDownChosen(_:))
        let prudential = DropDownMenuCell()
        prudential.textLabel!.text = "PRUDENTIAL"
        prudential.imageView!.image = #imageLiteral(resourceName: "issuer_logo_prudential")
        prudential.showsCheckmark = false
        prudential.menuTarget = self
        prudential.menuAction = #selector(NewPolicy2ViewController.issuerDropDownChosen(_:))
        
        let others = DropDownMenuCell()
        others.textLabel!.text = "OTHERS"
        others.imageView?.image = #imageLiteral(resourceName: "icon_clients")
        others.showsCheckmark = false
        others.menuTarget = self
        others.menuAction = #selector(NewPolicy2ViewController.issuerDropDownChosen(_:))
        
        issuerDropDown.menuCells = [aia, aviva, axa, greatEastern, manulife, prudential, others]
        issuerDropDown.direction = .down
        issuerDropDown.container = issuerDropDownViewFrame
    }
    
    @IBAction func issuerDropDownChosen(_ sender: AnyObject) {
        let dropDownMenuCell = sender as! DropDownMenuCell
        if let image = dropDownMenuCell.imageView?.image {
            issuerImageView.image = image
        }
        
        issuerShowButton.setTitle(dropDownMenuCell.textLabel?.text, for: .normal)
        showIssuerDropDownButtonTap(self)
    }
}


extension NewPolicy2ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        guard let rawText = textField.text else {
            return true
        }
        
        let fullString = NSString(string: rawText).replacingCharacters(in: range, with: string)
        
        
        if textField == policyEndAge {
            if Int(fullString) == nil {
                
            }
            else {
                if clientAge.text != "" {
                    let age: Int = Int(clientAge.text ?? "0")!
                    policyDurationYears = Int(fullString)! - age > 0 ? Int(fullString)! - age : 0
                    policyDuration.text = "\(policyDurationYears)"
                }
            }
        }
        
        if textField == policyDuration {
            if Int(fullString) != nil {
                if clientAge.text != "" {
                    guard let age: Int = Int(clientAge.text ?? "0"), let duration = Int(fullString) else {return true}
                    let endAge = age + duration
                    policyEndAge.text = "\(endAge)"
                }
            }
        }
        
        if textField == policyStartDate {
            guard let startDate = fullString.toDate() else {return true}
            var age = (startDate.year - client.dateOfBirth.year)
            if (startDate.month == client.dateOfBirth.month && startDate.day < client.dateOfBirth.day) {
                age -= 1
            }
            else if (startDate.month < client.dateOfBirth.month) {
                age -= 1
            }
            if age < 0 {age = 0}
            clientAge.text = "\(age)"
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print(textField.text as Any)
    }
}
