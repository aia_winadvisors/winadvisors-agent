//
//  BenefitsViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 5/11/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit
import MaterialComponents
import SwiftyJSON

class BenefitsViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var death: MDCTextField!
    var deathController: CustomMDCTextInputControllerOutlined!
    @IBOutlet weak var tpd: MDCTextField!
    var tpdController: CustomMDCTextInputControllerOutlined!
    @IBOutlet weak var disabilityIncome: MDCTextField!
    var disabilityIncomeController: CustomMDCTextInputControllerOutlined!
    @IBOutlet weak var critIllness: MDCTextField!
    var critIllnessController: CustomMDCTextInputControllerOutlined!
    @IBOutlet weak var earlyCritIllness: MDCTextField!
    var earlyCritIllnessController: CustomMDCTextInputControllerOutlined!
    @IBOutlet weak var accDeath: MDCTextField!
    var accDeathController: CustomMDCTextInputControllerOutlined!
    @IBOutlet weak var accTpd: MDCTextField!
    var accTpdController: CustomMDCTextInputControllerOutlined!
    @IBOutlet weak var accReimbursement: MDCTextField!
    var accReimbursementController: CustomMDCTextInputControllerOutlined!
    @IBOutlet weak var remarks: MDCTextField!
    var remarksController: CustomMDCTextInputControllerOutlined!
    @IBOutlet weak var hospital: MDCTextField!
    var hospitalController: CustomMDCTextInputControllerOutlined!
    @IBOutlet weak var hospitalIncome: MDCTextField!
    var hosptialIncomeController: CustomMDCTextInputControllerOutlined!
    var allTextFieldControllers = [CustomMDCTextInputControllerOutlined]()
    
    var callback: ((JSON, [[String:String]])->())?
    var benefits = [[String:String]]()
    var benefitsJSON: JSON = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
        
        // Do any additional setup after loading the view.
    }
    
    func setupTextFields() {
        deathController = CustomMDCTextInputControllerOutlined(textInput: death)
        death.delegate = self
        deathController.databaseIdentifier = "death"
        deathController.placeholderText = "Death"
        
        allTextFieldControllers.append(deathController)
        
        tpdController = CustomMDCTextInputControllerOutlined(textInput: tpd)
        tpd.delegate = self
        tpdController.placeholderText = "Total Permanent Disability"
        tpdController.databaseIdentifier = "tpd"
        allTextFieldControllers.append(tpdController)
        
        disabilityIncomeController = CustomMDCTextInputControllerOutlined(textInput: disabilityIncome)
        disabilityIncome.delegate = self
        disabilityIncomeController.placeholderText = "Disability Income"
        disabilityIncomeController.databaseIdentifier = "disability_income"
        allTextFieldControllers.append(disabilityIncomeController)
        
        critIllnessController = CustomMDCTextInputControllerOutlined(textInput: critIllness)
        critIllness.delegate = self
        critIllnessController.placeholderText = "Critical Illness"
        critIllnessController.databaseIdentifier = "crit_ill"
        allTextFieldControllers.append(critIllnessController)
        
        earlyCritIllnessController = CustomMDCTextInputControllerOutlined(textInput: earlyCritIllness)
        earlyCritIllness.delegate = self
        earlyCritIllnessController.placeholderText = "Early Critical Illness"
        earlyCritIllnessController.databaseIdentifier = "early_crit_ill"
        allTextFieldControllers.append(earlyCritIllnessController)
        
        accDeathController = CustomMDCTextInputControllerOutlined(textInput: accDeath)
        accDeath.delegate = self
        accDeathController.placeholderText = "Accidental Death"
        accDeathController.databaseIdentifier = "acc_death"
        allTextFieldControllers.append(accDeathController)
        
        accTpdController = CustomMDCTextInputControllerOutlined(textInput: accTpd)
        accTpd.delegate = self
        accTpdController.placeholderText = "Accidental Disability"
        accTpdController.databaseIdentifier = "acc_tpd"
        allTextFieldControllers.append(accTpdController)
        
        accReimbursementController = CustomMDCTextInputControllerOutlined(textInput: accReimbursement)
        accReimbursement.delegate = self
        accReimbursementController.placeholderText = "Accidental Reimbursement"
        accReimbursementController.databaseIdentifier = "acc_reimbursement"
        allTextFieldControllers.append(accReimbursementController)
        
        hospitalController = CustomMDCTextInputControllerOutlined(textInput: hospital)
        hospital.delegate = self
        hospitalController.placeholderText = "Hospitalisation"
        hospitalController.databaseIdentifier = "hosp"
        allTextFieldControllers.append(hospitalController)
        
        hosptialIncomeController = CustomMDCTextInputControllerOutlined(textInput: hospitalIncome)
        hospitalIncome.delegate = self
        hosptialIncomeController.placeholderText = "Hospital Income"
        hosptialIncomeController.databaseIdentifier = "hosp_income"
        allTextFieldControllers.append(hosptialIncomeController)
        
        remarksController = CustomMDCTextInputControllerOutlined(textInput: remarks)
        remarks.delegate = self
        remarksController.placeholderText = "Others"
        remarksController.databaseIdentifier = "others"
        allTextFieldControllers.append(remarksController)
        
        allTextFieldControllers.forEach { (textField) in
            //  textField.textInput?.text = self.benefits[textField.placeholderText!]
        }
        
        registerKeyboardNotifications()
    }
    
    
    //MARK: - Buttons
    
    @IBAction func submitTap(_ sender: UIButton) {
        self.benefits.removeAll()
        for benefit in allTextFieldControllers {
            if benefit.textInput?.text == "" {continue}
            if benefit.databaseIdentifier.contains("hosp") {
                benefits.append([
                    "name": benefit.placeholderText ?? "",
                    "type" : benefit.databaseIdentifier,
                    "remarks" : benefit.textInput!.text ?? "None"
                    ])
            }
            else {
                benefits.append([
                    "name": benefit.placeholderText ?? "",
                    "type" : benefit.databaseIdentifier,
                    "value" : benefit.textInput!.text ?? "0"
                    ])
            }
        }
        benefitsJSON = JSON.init(benefits)
        print(benefitsJSON)
        callback?(benefitsJSON, benefits)
        self.navigationController?.popViewController(animated: true)
    }
}




extension BenefitsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let index = textField.tag
        if index + 1 < allTextFieldControllers.count,
            let nextField = allTextFieldControllers[index + 1].textInput {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return false
    }
}

// MARK: - Keyboard Handling
extension BenefitsViewController {
    func registerKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow(notif:)),
            name: UIResponder.keyboardWillChangeFrameNotification,
            object: nil)
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow(notif:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillHide(notif:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }
    
    @objc func keyboardWillShow(notif: Notification) {
        guard let frame = notif.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
        scrollView.contentInset = UIEdgeInsets(top: 0.0,
                                               left: 0.0,
                                               bottom: frame.height,
                                               right: 0.0)
    }
    
    @objc func keyboardWillHide(notif: Notification) {
        scrollView.contentInset = UIEdgeInsets()
    }
}

public class CustomMDCTextInputControllerOutlined: MDCTextInputControllerOutlined {
    var databaseIdentifier: String = ""
}
