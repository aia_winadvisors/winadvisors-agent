//
//  NotificationListViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 18/1/19.
//  Copyright © 2019 Bilguun. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import SwiftyJSON
import SVProgressHUD

class NotificationCell: UITableViewCell {
    @IBOutlet weak var subject: UILabel!
    @IBOutlet weak var body: UILabel!
}

class NotificationListViewController: UIViewController {
    
    private var listOfNotifications = [NotificationModel]()
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        getNotifications()
        // Do any additional setup after loading the view.
    }
    
    func getNotifications() {
        SVProgressHUD.show()
        let url = Globals.shared.HOST_URL + Globals.Routes.getAgentNotifications + String(Agent.current.id)
        Alamofire.request(url).responseJSON { response in
            switch response.result {
            case .success(let value):
                let jsonArray = JSON(value).arrayValue
                for notification in jsonArray {
                  self.listOfNotifications.append(NotificationModel.init(unread: notification["unread"].boolValue, createdAt: notification["created_at"].stringValue.toDate(), senderId: notification["sender_id"].intValue, sender: notification["sender"].stringValue, subject: notification["subject"].stringValue, body: notification["body"].stringValue))
                }
                print(self.listOfNotifications)
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            case .failure(let error):
                print(error)
                SVProgressHUD.dismiss()
            }
        }
    }

    
}

extension NotificationListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.listOfNotifications.count == 0 {
            tableView.setEmptyMessage("No clients added yet. Please add clients or contact your admininstrator")
        }
        else {
            tableView.restore()
        }
        return listOfNotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NotificationCell
        cell.subject.text = self.listOfNotifications[indexPath.row].subject
        cell.body.text = self.listOfNotifications[indexPath.row].body
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
