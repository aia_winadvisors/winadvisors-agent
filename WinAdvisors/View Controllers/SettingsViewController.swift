//
//  SettingsViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 26/6/19.
//  Copyright © 2019 Bilguun. All rights reserved.
//

import UIKit
import ColorSlider

class SettingsViewController: UIViewController {
    
    //MARK: - Properties
    
    private var riskColor = UIColor.red
    private var wealthColor = UIColor.blue
    private var legacyColor = UIColor.green
    
    var preferences: SavedPreferences = .init(riskColorString: "", wealthColorString: "", legacyColorString: "", deathTpdOptValue: 10, critIllOptValue: 10, accDeathTpdOptValue: 100000)
    
    @IBOutlet weak var riskStackView: UIStackView!
    @IBOutlet weak var wealthStackView: UIStackView!
    @IBOutlet weak var legacyStackView: UIStackView!
    
    @IBOutlet weak var deathTpdOptTextField: UITextField!
    @IBOutlet weak var critIllOptTextField: UITextField!
    @IBOutlet weak var accDeathTpdTextField: UITextField!
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        getPrefs()
        // Do any additional setup after loading the view.
    }
    
    func getPrefs() {
        if let savedPrefs = UserDefaults.standard.object(forKey: "Preferences") as? Data {
            let decoder = JSONDecoder()
            if let loadedPrefs = try? decoder.decode(SavedPreferences.self, from: savedPrefs) {
                print(loadedPrefs)
                setup(with: [UIColor.UIColorFromString(string: loadedPrefs.riskColorString), UIColor.UIColorFromString(string: loadedPrefs.wealthColorString), UIColor.UIColorFromString(string: loadedPrefs.legacyColorString)])
            }
        }
        else {
            setup()
        }
    }
    
    func setup(with colors: [UIColor] = []) {
        
        let riskSlider = ColorSlider(orientation: .horizontal, previewSide: .top)
        riskSlider.tag = 0
        riskSlider.addTarget(self, action: #selector(changedColor(_:)), for: .valueChanged)
        riskStackView.addArrangedSubview(riskSlider)
        
        let wealthSlider = ColorSlider(orientation: .horizontal, previewSide: .top)
        wealthSlider.tag = 1
        wealthSlider.addTarget(self, action: #selector(changedColor(_:)), for: .valueChanged)
        wealthStackView.addArrangedSubview(wealthSlider)
        
        let legacySlider = ColorSlider(orientation: .horizontal, previewSide: .top)
        legacySlider.tag = 2
        legacySlider.addTarget(self, action: #selector(changedColor(_:)), for: .valueChanged)
        legacyStackView.addArrangedSubview(legacySlider)
        
        if colors.count > 0 {
            riskSlider.color = colors[0]
            wealthSlider.color = colors[1]
            legacySlider.color = colors[2]
        }
        
    }
    
    @objc func changedColor(_ slider: ColorSlider) {
        switch slider.tag {
        case 0:
            riskColor = slider.color
        case 1:
            wealthColor = slider.color
        case 2:
            legacyColor = slider.color
        default:
            riskColor = slider.color
        }
    }
    
    
    //MARK: - Buttons
    @IBAction func didTapSave(_ sender: Any) {
        
        guard let deathTpdOptText = deathTpdOptTextField.text, let deathTpdValue = Double(deathTpdOptText), let critIllOptText = critIllOptTextField.text, let critIllValue = Double(critIllOptText), let accDeathTpdOptText = accDeathTpdTextField.text, let accDeathTpdValue = Double(accDeathTpdOptText) else {return}
        
        preferences = SavedPreferences.init(riskColorString: UIColor.StringFromUIColor(color: riskColor), wealthColorString: UIColor.StringFromUIColor(color: wealthColor), legacyColorString: UIColor.StringFromUIColor(color: legacyColor), deathTpdOptValue: deathTpdValue, critIllOptValue: critIllValue, accDeathTpdOptValue: accDeathTpdValue)
        
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(preferences) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: "Preferences")
            Agent.current.updatePreferences()
        }
    }
    
}

struct SavedPreferences: Codable {
    var riskColorString: String
    var wealthColorString: String
    var legacyColorString: String
    var deathTpdOptValue: Double
    var critIllOptValue: Double
    var accDeathTpdOptValue: Double
}
