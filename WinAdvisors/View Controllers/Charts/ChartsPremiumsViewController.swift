//
//  ChartsPremiumsViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 29/11/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit
import Charts
import SwiftyJSON
import SwiftDate
import MaterialComponents.MaterialTypography
import UserNotifications

class ChartsPremiumsViewController: BaseChartViewController {
    
    @IBOutlet weak var premiumsChartView: CombinedChartView!
    @IBOutlet weak var coverageChartView: LineChartView!
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var incomeRatioView: IncomeRatioView!
    @IBOutlet weak var premiumsChartButton: UIButton!
    @IBOutlet weak var coverageChartButton: UIButton!
    
    let chartLabels = ["Protection", "Wealth", "Coverage", "Investments", "Savings", "Cash Value"]
    var client: ClientModel?
    var premiums:JSON = []
    private var chartDataEntry = [ChartDataEntry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupChart()
        setupPieChart()
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(onUpdatePolicyDetails(_:)), name: .didUpdatePolicyDetails, object: nil)
    }
    
    @objc func onUpdatePolicyDetails(_ notification:Notification) {
        self.getIncomeRatio()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        calculateAnnualPremiums()
        self.getIncomeRatio()
    }
    
    override func updateChartData() {
        if self.shouldHideData {
            premiumsChartView.data = nil
            return
        }
    
    }
    
    func calculateAnnualPremiums() {
        guard let client = client else {return}
        client.getPolicies(completion: { (policies) in
            var protectionPremiums = [Int: Double]()
            var wealthPremiums = [Int: Double]()
            var coverageDeath = [Int: Double]()
            var coverageTPD = [Int: Double]()
            var coverageCritIll = [Int: Double]()
            var couponPayment = [Int: Double]()
            var cashValues = [Int: Double]()
            let leftAxis = self.premiumsChartView.leftAxis
            leftAxis.removeAllLimitLines()
            leftAxis.gridLineDashLengths = [5, 5]
            let rightAxis = self.premiumsChartView.rightAxis
            rightAxis.removeAllLimitLines()
            rightAxis.gridLineDashLengths = [5, 5]
            
            for policy in policies {
                for i in client.age...policy.premiumEndAge {
                    
                    //protection premiums
                    if policy.policyType == "risk" {
                        if protectionPremiums[i] == nil {
                            protectionPremiums[i] = 0
                        }
                        protectionPremiums[i]! += policy.annualPremium
                        
                        if protectionPremiums[i]! > leftAxis.axisMaximum {
                            leftAxis.axisMaximum = protectionPremiums[i]! * 1.1
                        }
                        
                        if protectionPremiums[i]! < leftAxis.axisMinimum {
                            leftAxis.axisMinimum = protectionPremiums[i]! * 0.8
                        }
                        
                    }
                    //wealth premiums
                    else {
                        if wealthPremiums[i] == nil {
                            wealthPremiums[i] = 0
                        }
                        wealthPremiums[i]! += policy.annualPremium
                        
                        if wealthPremiums[i]! > leftAxis.axisMaximum {
                            leftAxis.axisMaximum = wealthPremiums[i]! * 1.1
                        }
                        
                        if wealthPremiums[i]! < leftAxis.axisMinimum {
                            leftAxis.axisMinimum = wealthPremiums[i]! * 0.8
                        }
                    }
                    
                    if Double(i) >= self.premiumsChartView.xAxis.axisMaximum {
                        self.premiumsChartView.xAxis.axisMaximum = Double(i + 1)
                    }
                    if Double(i) <= self.premiumsChartView.xAxis.axisMinimum {
                        self.premiumsChartView.xAxis.axisMinimum = Double(i - 1)
                    }
                    
                }
                
                leftAxis.axisMinimum = 0
                rightAxis.axisMinimum = 0
                
                for i in 0...policy.policyDuration {
                    let benefits = policy.benefits.arrayValue
                    for benefit in benefits {
                        let type = benefit["type"].stringValue
                        if type == "death" {
                            if coverageDeath[client.age + i] == nil {
                                coverageDeath[client.age + i] = 0
                            }
                            
                            coverageDeath[client.age + i]! += benefit["value"].doubleValue
                        
                        }
                        
                        if type == "tpd" {
                            if coverageTPD[client.age + i] == nil {
                                coverageTPD[client.age + i] = 0
                            }
                            
                            coverageTPD[client.age + i]! += benefit["value"].doubleValue
                            
                        }
                        
                        if type.contains("crit_ill") {
                            if coverageCritIll[client.age + i] == nil {
                                coverageCritIll[client.age + i] = 0
                            }
                            
                            coverageCritIll[client.age + i]! += benefit["value"].doubleValue
                            
                        }
                        
                    }
                }
                
                if policy.cashValueAge > 0 && policy.cashValueAmount > 0 {
                    if cashValues[policy.cashValueAge] == nil {
                        cashValues[policy.cashValueAge] = 0
                    }
                    cashValues[policy.cashValueAge]! += policy.cashValueAmount
                    
                    if cashValues[policy.cashValueAge]! > rightAxis.axisMaximum {
                        rightAxis.axisMaximum = cashValues[policy.cashValueAge]! * 1.1
                    }
                    
                    if cashValues[policy.cashValueAge]! < rightAxis.axisMinimum {
                        rightAxis.axisMinimum = cashValues[policy.cashValueAge]! * 0.8
                    }
                }
                
                guard let couponEndDate = policy.couponEndDate else {continue}
                let couponDuration = couponEndDate.year - DateInRegion().year
                
                for i in client.age...couponDuration + client.age {
                    if couponPayment[i] == nil {
                        couponPayment[i] = 0
                    }
                    couponPayment[i]! += policy.couponAmount
                    
                    if couponPayment[i]! > leftAxis.axisMaximum {
                        leftAxis.axisMaximum = couponPayment[i]! * 1.1
                    }
                    
                    if couponPayment[i]! < leftAxis.axisMinimum {
                        leftAxis.axisMinimum = couponPayment[i]! * 0.8
                    }
                }
            }
            
            var protectionData = [ChartDataEntry]()
            var wealthData = [ChartDataEntry]()
            var couponData = [ChartDataEntry]()
            var cashValuesData = [BarChartDataEntry]()
            
            for (age, premium) in protectionPremiums.sorted(by: {$0.key < $1.key})   {
                protectionData.append(ChartDataEntry(x: (Double(age)), y: (premium)))
            }
            
            for (age, premium) in wealthPremiums.sorted(by: {$0.key < $1.key})   {
                wealthData.append(ChartDataEntry(x: (Double(age)), y: (premium)))
            }
            
            for (age, payment) in couponPayment.sorted(by: {$0.key < $1.key}) {
                couponData.append(ChartDataEntry(x: (Double(age)), y: (payment)))
            }
            
            for (age, payment) in cashValues.sorted(by: {$0.key < $1.key}) {
                cashValuesData.append(BarChartDataEntry(x: (Double(age)), y: (payment)))
            }
            
            let protectionSet = LineChartDataSet(entries: protectionData, label: "Protection Premiums")
            protectionSet.drawIconsEnabled = false
            protectionSet.setColor(UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1))
            protectionSet.lineWidth = 2
            protectionSet.setCircleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            protectionSet.circleRadius = 3
            protectionSet.drawCircleHoleEnabled = false
            protectionSet.valueFont = .systemFont(ofSize: 18)
            protectionSet.formSize = 15
            protectionSet.mode = .linear
            protectionSet.fillColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
            protectionSet.drawValuesEnabled = false
            protectionSet.drawFilledEnabled = false
            
            let wealthSet = LineChartDataSet(entries: wealthData, label: "Wealth Premiums")
            wealthSet.drawIconsEnabled = false
            wealthSet.lineDashLengths = [5, 2.5]
            wealthSet.highlightLineDashLengths = [5, 2.5]
            wealthSet.setColor(.black)
            wealthSet.lineWidth = 2
            wealthSet.setCircleColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1))
            wealthSet.circleRadius = 3
            wealthSet.drawCircleHoleEnabled = false
            wealthSet.valueFont = .systemFont(ofSize: 18)
            wealthSet.formLineDashLengths = [5, 2.5]
            wealthSet.formLineWidth = 1
            wealthSet.formSize = 15
            wealthSet.mode = .linear
            wealthSet.setColor(#colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1))
            wealthSet.drawValuesEnabled = false//.linearGradient(gradient, angle: 90)
            
            let couponSet = LineChartDataSet(entries: couponData, label: "Coupon Payments")
            couponSet.drawIconsEnabled = false
            couponSet.lineDashLengths = [5, 2.5]
            couponSet.highlightLineDashLengths = [5, 2.5]
            couponSet.setColor(.black)
            couponSet.lineWidth = 2
            couponSet.setCircleColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1))
            couponSet.circleRadius = 3
            couponSet.drawCircleHoleEnabled = false
            couponSet.valueFont = .systemFont(ofSize: 18)
            couponSet.formLineDashLengths = [5, 2.5]
            couponSet.formLineWidth = 1
            couponSet.formSize = 15
            couponSet.mode = .linear
            couponSet.setColor(#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1))
            couponSet.drawValuesEnabled = false//.linearGradient(gradient, angle: 90)
            
            let cashValuesSet = BarChartDataSet(entries: cashValuesData, label: "Cash Value")
            cashValuesSet.setColor(#colorLiteral(red: 0, green: 1, blue: 0.7605624795, alpha: 1))
            cashValuesSet.valueTextColor = UIColor(red: 60/255, green: 220/255, blue: 78/255, alpha: 1)
            cashValuesSet.valueFont = .systemFont(ofSize: 10)
            cashValuesSet.axisDependency = .right
            
            let premiumLineData = LineChartData(dataSets: [protectionSet, wealthSet, couponSet])
            let premiumBarData = BarChartData(dataSets: [cashValuesSet])
            let combinedData = CombinedChartData()
            combinedData.lineData = premiumLineData
            combinedData.barData = premiumBarData
            self.premiumsChartView.data = combinedData
            self.premiumsChartView.setNeedsDisplay()
            self.premiumsChartView.animate(xAxisDuration: 2.5)
            
            
            
            var coverageDeathDataEntries = [ChartDataEntry]()
            var coverageTPDDataEntries = [ChartDataEntry]()
            var coverageCritIllDataEntries = [ChartDataEntry]()
            
            for (age, coverage) in coverageDeath.sorted(by: {$0.key < $1.key}) {
                coverageDeathDataEntries.append(ChartDataEntry(x: (Double(age)), y: (coverage)))
            }
            for (age, coverage) in coverageTPD.sorted(by: {$0.key < $1.key}) {
                coverageTPDDataEntries.append(ChartDataEntry(x: (Double(age)), y: (coverage)))
            }
            for (age, coverage) in coverageCritIll.sorted(by: {$0.key < $1.key}) {
                coverageCritIllDataEntries.append(ChartDataEntry(x: (Double(age)), y: (coverage)))
            }
            
            let coverageDeathSet = LineChartDataSet(entries: coverageDeathDataEntries, label: "Death")
            coverageDeathSet.drawIconsEnabled = false
            coverageDeathSet.lineDashLengths = [5, 2.5]
            coverageDeathSet.highlightLineDashLengths = [5, 2.5]
            coverageDeathSet.setColor(.black)
            coverageDeathSet.lineWidth = 2
            coverageDeathSet.setCircleColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1))
            coverageDeathSet.circleRadius = 3
            coverageDeathSet.drawCircleHoleEnabled = false
            coverageDeathSet.valueFont = .systemFont(ofSize: 18)
            coverageDeathSet.formLineDashLengths = [5, 2.5]
            coverageDeathSet.formLineWidth = 1
            coverageDeathSet.formSize = 15
            coverageDeathSet.mode = .linear
            coverageDeathSet.setColor(#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
            coverageDeathSet.drawValuesEnabled = false//.linearGradient(gradient, angle: 90)
            
            let coverageTPDSet = LineChartDataSet(entries: coverageTPDDataEntries, label: "TPD")
            coverageTPDSet.drawIconsEnabled = false
            coverageTPDSet.lineDashLengths = [5, 2.5]
            coverageTPDSet.highlightLineDashLengths = [5, 2.5]
            coverageTPDSet.setColor(.black)
            coverageTPDSet.lineWidth = 2
            coverageTPDSet.setCircleColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1))
            coverageTPDSet.circleRadius = 3
            coverageTPDSet.drawCircleHoleEnabled = false
            coverageTPDSet.valueFont = .systemFont(ofSize: 18)
            coverageTPDSet.formLineDashLengths = [5, 2.5]
            coverageTPDSet.formLineWidth = 1
            coverageTPDSet.formSize = 15
            coverageTPDSet.mode = .linear
            coverageTPDSet.setColor(#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1))
            coverageTPDSet.drawValuesEnabled = false//.linearGradient(gradient, angle: 90)
            
            let coverageCritIllSet = LineChartDataSet(entries: coverageCritIllDataEntries, label: "Crit Ill")
            coverageCritIllSet.drawIconsEnabled = false
            coverageCritIllSet.lineDashLengths = [5, 2.5]
            coverageCritIllSet.highlightLineDashLengths = [5, 2.5]
            coverageCritIllSet.setColor(.black)
            coverageCritIllSet.lineWidth = 2
            coverageCritIllSet.setCircleColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1))
            coverageCritIllSet.circleRadius = 3
            coverageCritIllSet.drawCircleHoleEnabled = false
            coverageCritIllSet.valueFont = .systemFont(ofSize: 18)
            coverageCritIllSet.formLineDashLengths = [5, 2.5]
            coverageCritIllSet.formLineWidth = 1
            coverageCritIllSet.formSize = 15
            coverageCritIllSet.mode = .linear
            coverageCritIllSet.setColor(#colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1))
            coverageCritIllSet.drawValuesEnabled = false//.linearGradient(gradient, angle: 90)
            
            
            let coverageData = LineChartData(dataSets: [coverageDeathSet, coverageTPDSet, coverageCritIllSet])
            self.coverageChartView.data = coverageData
            self.coverageChartView.setNeedsDisplay()
            self.coverageChartView.animate(xAxisDuration: 2.5)
        })
        
    }
    
    func getIncomeRatio() {
        guard let clientId = client?.id, let annualIncome = client?.netAnnualIncome  else {return}
        var riskPayment = 0.0
        var wealthPayment = 0.0
        let livingExpenses = client?.livingExpenses ?? 0.0
        let shortTermSavings = client?.shortTermSavings ?? 0.0
        NetworkUtility.shared.HTTPGet(route: "\(Globals.Routes.baseClientRoute)\(clientId)/getIncomeRatio") { (response) in
            print(response)
            for (_,subJson):(String, JSON) in response {
                switch subJson["type"] {
                case "risk":
                    riskPayment = subJson["annualPremium"].doubleValue
                case "wealth_accum":
                    wealthPayment = subJson["annualPremium"].doubleValue
                default:
                    return
                }
            }
            let incomeRatio = IncomeRatio.init(insuranceCover: riskPayment, wealthAccumulation: wealthPayment, shortTermSavings: shortTermSavings, livingExp: livingExpenses, annualIncome: annualIncome)
            var entries = [PieChartDataEntry]()
            entries.append(PieChartDataEntry.init(value: incomeRatio.insuranceCover))
            entries.append(PieChartDataEntry.init(value: incomeRatio.wealthAccumulation))
            entries.append(PieChartDataEntry.init(value: incomeRatio.shortTermSavings))
            entries.append(PieChartDataEntry.init(value: incomeRatio.livingExp))
            entries.append(PieChartDataEntry.init(value: incomeRatio.unusedIncome))
            
            let set = PieChartDataSet(entries: entries, label: "")
            set.sliceSpace = 0
            set.drawValuesEnabled = true
            
            //med #colorLiteral(red: 0.2682668567, green: 0.7741906643, blue: 0.5486420393, alpha: 1),
            // short
            
            set.colors = [Agent.current.agentPreferences.riskColor, Agent.current.agentPreferences.wealthColor, #colorLiteral(red: 0.4238357544, green: 0.6881807446, blue: 0.7324489951, alpha: 1), #colorLiteral(red: 0.8792218566, green: 0.1968622506, blue: 0.3102308512, alpha: 1), #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)]
            
            let data = PieChartData(dataSet: set)
            let pFormatter = NumberFormatter()
            pFormatter.numberStyle = .percent
            pFormatter.maximumFractionDigits = 1
            pFormatter.multiplier = 1
            pFormatter.percentSymbol = " %"
            pFormatter.zeroSymbol = ""
            data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
            self.pieChartView.data = data
            self.pieChartView.highlightValues(nil)
            self.incomeRatioView.configure(incomeRatio: incomeRatio)
            self.pieChartView.setNeedsDisplay()
        }
    }
    
    
    //MARK: - Buttons
    
    @IBAction func toggleCharts(_ sender: UIButton) {
        sender.isSelected = true
        switch sender.tag {
        case 0:
            premiumsChartView.isHidden = false
            coverageChartView.isHidden = true
            coverageChartButton.isSelected = false
        case 1:
            premiumsChartView.isHidden = true
            coverageChartView.isHidden = false
            premiumsChartButton.isSelected = false
        default:
            premiumsChartView.isHidden = false
            coverageChartView.isHidden = true
            coverageChartButton.isSelected = false
        }
    }
    
}

extension ChartsPremiumsViewController {
    
    
    func setupPieChart() {
        pieChartView.delegate = self
        pieChartView.usePercentValuesEnabled = true
        pieChartView.drawSlicesUnderHoleEnabled = false
        pieChartView.holeRadiusPercent = 0.50
        pieChartView.transparentCircleRadiusPercent = 0.70
        pieChartView.chartDescription?.enabled = false
        pieChartView.drawCenterTextEnabled = false
        pieChartView.drawHoleEnabled = true
        pieChartView.rotationEnabled = false
        pieChartView.highlightPerTapEnabled = false
        pieChartView.drawEntryLabelsEnabled = true
        pieChartView.legend.enabled = false
    }
    
    func setPieChartDataCount(_ count: Int, range: UInt32) {
        
    }
    
    
    func setupChart() {
        premiumsChartView.delegate = self
        
        premiumsChartView.chartDescription?.enabled = false
        premiumsChartView.dragEnabled = true
        premiumsChartView.setScaleEnabled(true)
        premiumsChartView.pinchZoomEnabled = true
        premiumsChartView.xAxis.gridLineDashLengths = [10, 10]
        premiumsChartView.xAxis.gridLineDashPhase = 0
        premiumsChartView.xAxis.labelPosition = .bottom
        premiumsChartView.rightAxis.enabled = true
        premiumsChartView.leftAxis.enabled = true
        premiumsChartView.legend.form = .line
        premiumsChartView.legend.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        
//        let marker = BalloonMarker(color: UIColor(white: 180/255, alpha: 1),
//                                   font: .systemFont(ofSize: 12),
//                                   textColor: .white,
//                                   insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
//        marker.chartView = self.premiumsChartView
//        marker.minimumSize = CGSize(width: 80, height: 40)
//        self.premiumsChartView.marker = marker
        
        coverageChartView.delegate = self
        
        coverageChartView.chartDescription?.enabled = false
        coverageChartView.dragEnabled = true
        coverageChartView.setScaleEnabled(true)
        coverageChartView.pinchZoomEnabled = true
        coverageChartView.xAxis.gridLineDashLengths = [10, 10]
        coverageChartView.xAxis.gridLineDashPhase = 0
        coverageChartView.xAxis.labelPosition = .bottom
        coverageChartView.rightAxis.enabled = false
        coverageChartView.leftAxis.enabled = true
        coverageChartView.legend.form = .line
        coverageChartView.legend.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        
        let coverageMarker = BalloonMarker(color: UIColor(white: 180/255, alpha: 1),
                                   font: .systemFont(ofSize: 12),
                                   textColor: .white,
                                   insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        coverageMarker.chartView = self.coverageChartView
        coverageMarker.minimumSize = CGSize(width: 80, height: 40)
        self.coverageChartView.marker = coverageMarker
        updateChartData()
    }
}


struct IncomeRatio {
    //  let health: Double
    let insuranceCover: Double
    let wealthAccumulation: Double
    let shortTermSavings: Double
    let livingExp: Double
    let annualIncome: Double
    let unusedIncome: Double
    
    init (insuranceCover:Double, wealthAccumulation: Double, shortTermSavings: Double, livingExp: Double, annualIncome: Double) {
        self.insuranceCover = insuranceCover
        self.wealthAccumulation = wealthAccumulation
        self.shortTermSavings = shortTermSavings
        self.livingExp = livingExp
        self.annualIncome = annualIncome
        self.unusedIncome = annualIncome - insuranceCover - wealthAccumulation - shortTermSavings - livingExp
    }
    
}
