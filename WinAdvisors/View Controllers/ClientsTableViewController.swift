//
//  ContactsViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 1/2/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit
import SideMenu
import SwiftyJSON
import Alamofire
import SVProgressHUD
import MessageUI


class ClientsViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    private let refreshControl = UIRefreshControl()
    
    @IBOutlet weak var clientsTableView: UITableView!
    
    var listOfClients: [ClientModel] = []
    var sortedListOfClients: [ClientModel] = []
    var selectedClient: ClientModel?
    var selectedClientViewModel: ClientViewModel?
    var clientSection = [String]()
    var clientDictionary = [String:[ClientModel]]()
    
    private var isSortedByName = false
    private var isSortedByBirthday = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
        clientsTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(getCustomers), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching clients ...", attributes: nil)
        navigationController?.isNavigationBarHidden = false
        clientsTableView.sectionIndexBackgroundColor = .white
        clientsTableView.register(UINib(nibName: "ClientTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        clientsTableView.delegate = self
        clientsTableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let headerView = ClientTableHeaderView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        headerView.delegate = self
        clientsTableView.tableHeaderView = headerView
        getCustomers()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showClient" {
            let destination = segue.destination as! ClientViewController
            destination.client = selectedClient!
            destination.clientViewModel = selectedClientViewModel
        }
    }
    
    @objc func getCustomers() {
        listOfClients.removeAll()
        self.clientDictionary.removeAll()
        let url = Globals.shared.HOST_URL + Globals.Routes.agentGetClients
        Alamofire.request(url, headers: ["x-auth-token": Agent.current.idToken]).responseJSON { response in
            switch response.result {
            case .success(let value):
                let jsonArray = JSON(value).arrayValue
                for client in jsonArray {
                    let client = (Mapping.ClientMap(clientJSON: client))
                    self.listOfClients.append(client)
                    let key = "\(client.name[client.name.startIndex])".uppercased()
                    if let _ = self.clientDictionary[key] {
                        self.clientDictionary[key]?.append(client)
                    } else {
                        self.clientDictionary[key] = [client]
                    }
                    self.clientSection = [String](self.clientDictionary.keys).sorted()
                    
                }
                print(self.listOfClients)
                self.navigationItem.title = "Clients: \(self.listOfClients.count)"
                Agent.current.clients = self.listOfClients
                self.sortedListOfClients = self.listOfClients
                self.clientsTableView.reloadData()
                SVProgressHUD.dismiss()
                self.refreshControl.endRefreshing()
            case .failure(let error):
                print(error)
                SVProgressHUD.dismiss()
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func sendEmail(to client: ClientModel) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([client.email])
            mail.setSubject("Greetings!")
            mail.setMessageBody("<p>Dear \(client.name.capitalized), </p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            showDefaultAlert(message: "No default email client setup")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func showDefaultAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Buttons
    
    @IBAction func ShowSideMenu(_ sender: Any) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func massEmailDidtap(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            let clients = getAllClientEmails()
            mail.setCcRecipients(clients)
            mail.setSubject("Greetings!")
            mail.setMessageBody("<p>Dear __, </p>", isHTML: true)
        }
    }
    
    private func getAllClientEmails() -> [String] {
        var clientList = [String]()
        for client in Agent.current.clients {
            clientList.append(client.email)
        }
        
        return clientList
    }
    
}

extension ClientsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty && searchText.count > 1 {
            sortedListOfClients = self.listOfClients.filter({ (client) -> Bool in
                return client.name.lowercased().contains(searchText.lowercased())
            })
            
            clientsTableView.reloadData()
        }
        
        if searchText.isEmpty {
            sortedListOfClients = self.listOfClients
            clientsTableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}

extension ClientsViewController: UITableViewDelegate, UITableViewDataSource, ClientTableHeaderViewProtocol {
    
    
    //Sorting:
    
    func sortBy(option: SortClientBy) {
        switch option {
        case .name:
            sortedListOfClients = self.listOfClients.sorted(by: { (first: ClientModel, second: ClientModel) -> Bool in
                if self.isSortedByName {
                    return first.name < second.name
                }
                else {
                    return first.name > second.name
                }
            })
            self.isSortedByName = !self.isSortedByName
        case .lastPurchaseDate:
            sortedListOfClients = self.listOfClients
        case .nextContactDate:
            sortedListOfClients = self.listOfClients
        case .birthday:
            sortedListOfClients = self.listOfClients.sorted(by: { (first: ClientModel, second: ClientModel) -> Bool in
                if self.isSortedByBirthday {
                    return first.dateOfBirth.month < second.dateOfBirth.month
                }
                else {
                    return first.dateOfBirth.month > second.dateOfBirth.month
                }
            })
            self.isSortedByBirthday = !self.isSortedByBirthday
        }
        
        self.clientsTableView.reloadData()
    }
    
    //MARK: - Tables
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
    //        return clientSection
    //    }
    
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        return clientSection[section].uppercased()
    //    }
    //
    //    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    //        view.tintColor = #colorLiteral(red: 0.3333333333, green: 0.6392156863, blue: 0.9333333333, alpha: 1)
    //        let header = view as! UITableViewHeaderFooterView
    //        header.textLabel?.font = UIFont.systemFont(ofSize: 24, weight: .heavy)
    //        header.textLabel?.textColor = UIColor.white
    //    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        print("list of clients in number of section is \(listOfClients.count)")
        //        if self.listOfClients.count == 0 {
        //            tableView.setEmptyMessage("No clients added yet. Please add clients or contact your admininstrator")
        //            return 0
        //        }
        //        else {
        //            tableView.restore()
        //        }
        //        let clientKey = clientSection[section]
        //        if let clients = clientDictionary[clientKey] {
        //            return clients.count
        //        }
        //        return 0
        return sortedListOfClients.count
    }
    
    //    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
    //
    //        let deleteAction = UIContextualAction(style: .normal, title: nil) { action, view, complete in
    //            print("deleting at \(indexPath.row)!")
    //            complete(true)
    //        }
    //
    //        deleteAction.image = #imageLiteral(resourceName: "icon_cancel")
    //
    //        let emailAction = UIContextualAction(style: .normal, title: nil) { action, view, complete in
    //            print("emailing at \(indexPath.row)")
    //            complete(true)
    //        }
    //        emailAction.image = #imageLiteral(resourceName: "icon_mail")
    //
    //        return UISwipeActionsConfiguration(actions: [deleteAction, emailAction])
    //    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            self.sortedListOfClients[indexPath.row].deleteClient()
            self.sortedListOfClients.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
        
        let email = UITableViewRowAction(style: .default, title: "Email") { (action, indexPath) in
            let client = self.sortedListOfClients[indexPath.row]
            self.sendEmail(to: client)
        }
        
        email.backgroundColor = UIColor.lightGray
        
        return [delete, email]
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ClientTableViewCell
        //        let clientKey = clientSection[indexPath.section]
        //        if let clients = clientDictionary[clientKey.uppercased()] {
        //            cell.configure(with: clients[indexPath.row])
        //        }
        
        cell.configure(with: sortedListOfClients[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let clientKey = clientSection[indexPath.section]
        //        if let clients = clientDictionary[clientKey.uppercased()] {
        //            selectedClient = clients[indexPath.row]
        //        }
        //
        //        guard let selectedClient = selectedClient else {return}
        selectedClient = sortedListOfClients[indexPath.row]
        if selectedClient != nil {
            selectedClientViewModel = ClientViewModel(client: selectedClient!)
            performSegue(withIdentifier: "showClient", sender: self)
        }
    }
}
