//
//  SideMenuViewController.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 3/2/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    
    @IBOutlet weak var iconHome: UIImageView!
    @IBOutlet weak var iconClients: UIImageView!
    @IBOutlet weak var iconNotification: UIImageView!
    @IBOutlet weak var iconSettings: UIImageView!
    
    var currentHighlightedIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        highlightIcon()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func highlightIcon() {
        switch Globals.shared.CurrentSideMenu {
        case .Home:
            iconHome.isHighlighted = true
        case .Clients:
            iconClients.isHighlighted = true
        case .Notification:
            iconNotification.isHighlighted = true
        case .Settings:
            iconSettings.isHighlighted = true
        default:
            iconHome.isHighlighted = true
        }
    }
    
    @IBAction func ShowHome(_ sender: Any) {
        Navigate(storyBoardName: "Home")
        iconHome.isHighlighted = true
        Globals.shared.CurrentSideMenu = .Home
    }
    
    @IBAction func ShowContacts(_ sender: Any) {
        Navigate(storyBoardName: "Clients")
        iconClients.isHighlighted = true
        Globals.shared.CurrentSideMenu = .Clients
    }
    
    @IBAction func showSettings(_ sender: Any) {
        Navigate(storyBoardName: "Settings")
        iconSettings.isHighlighted = true
        Globals.shared.CurrentSideMenu = .Settings
    }
    
    @IBAction func ShowNotification(_ sender: Any) {
        Navigate(storyBoardName: "Notification")
        iconNotification.isHighlighted = true
        Globals.shared.CurrentSideMenu = .Notification
        
    }
    
    func Navigate(storyBoardName:String) {
        let newVC = storyboard!.instantiateViewController(withIdentifier: storyBoardName)
        Globals.shared.MainNavController?.setViewControllers([newVC], animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
