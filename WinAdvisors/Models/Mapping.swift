//
//  Mapping.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 19/6/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import Foundation
import SwiftyJSON

class Mapping {
    
    static func ClientMap(clientJSON: JSON) -> ClientModel {
        return ClientModel(id: clientJSON["_id"].stringValue, name: clientJSON["name"].stringValue, prefName: clientJSON["prefName"].stringValue, nric: clientJSON["nric"].stringValue, clientType: clientJSON["clientType"].stringValue, email: clientJSON["email"].stringValue, dateOfBirth: clientJSON["dateOfBirth"].stringValue, gender: clientJSON["gender"].stringValue, race: clientJSON["race"].stringValue, nationality: clientJSON["nationality"].stringValue, remark: clientJSON["remark"].stringValue, handphone: clientJSON["handphone"].stringValue, address1: clientJSON["address1"].stringValue, address2: clientJSON["address2"].stringValue, postalCode: clientJSON["postalCode"].stringValue, annualIncome: clientJSON["annualIncome"].doubleValue, netAnnualIncome: clientJSON["netAnnualIncome"].doubleValue, shortTermSavings: clientJSON["shortTermSavings"].doubleValue, livingExpenses: clientJSON["livingExpenses"].doubleValue, agent: clientJSON["agent"].stringValue, familyOf: clientJSON["familyOf"].stringValue, relationship: clientJSON["relationship"].stringValue, policies: clientJSON["policies"], lastPurchased: clientJSON["lastPurchased"].stringValue, nextContact: clientJSON["nextContact"].stringValue)
    }
}

