//
//  Clients.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 8/6/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import Foundation
import SwiftDate

class Customer: NSObject {
    
    let id: Int
    let name: String
    let nric: String
    let date_of_birth: DateInRegion
    let gender: String
    let race: String
    let nationality: String
    let remark: String
    let handphone: String
    let address: String
    let agent_id: Int
    
    
    init(id:Int, name: String, nric: String, date_of_birth: String, gender: String, race: String, nationality:String, remark: String, handphone: String, address: String, agent_id: Int) {
        self.id = id
        self.name = name
        self.nric = nric
        self.date_of_birth = date_of_birth.toDate("yyyy-MM-dd")!
        self.gender = gender
        self.race = race
        self.nationality = nationality
        self.remark = remark
        self.handphone = handphone
        self.address = address
        self.agent_id = agent_id
    }
}
