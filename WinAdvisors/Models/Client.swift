//
//  Clients.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 8/6/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import Foundation
import SwiftDate
import SwiftyJSON
import Alamofire


//MARK: - Model
class ClientModel: NSObject {
    
    let id: String
    var name: String
    var prefName: String
    var clientType: String
    var nric: String
    var email: String
    var dateOfBirth: DateInRegion
    var age: Int
    var gender: String
    var race: String
    var nationality: String
    var remark: String
    var handphone: String
    var address1: String
    var address2: String
    var postalCode: String
    var familyOf: String
    var familyOfName: String = ""
    var relationship: String
    var shortTermSavings: Double
    var livingExpenses: Double
    var annualIncome: Double?
    var netAnnualIncome: Double?
    var agent: String
    var policies: JSON
    var lastPurchased: DateInRegion?
    var nextContact: DateInRegion
    
    var policyList = [PolicyModel]()
    
    init(id:String, name: String, prefName: String, nric: String, clientType: String, email: String, dateOfBirth: String, gender: String, race: String, nationality:String, remark: String, handphone: String, address1: String, address2: String, postalCode: String, annualIncome: Double? = 0, netAnnualIncome: Double? = 0, shortTermSavings: Double, livingExpenses: Double, agent: String, familyOf: String, relationship: String, policies: JSON, lastPurchased: String, nextContact: String) {
        self.id = id
        self.name = name
        self.prefName = prefName
        self.clientType = clientType
        self.nric = nric
        self.email = email
        self.dateOfBirth = dateOfBirth.toDate()!
        let now = DateInRegion.init(Date(), region: Region.current)
        self.age = now.year - self.dateOfBirth.year
        self.gender = gender
        self.race = race
        self.familyOf = familyOf
        self.relationship = relationship
        self.nationality = nationality
        self.remark = remark
        self.handphone = handphone
        self.address1 = address1
        self.address2 = address2
        self.postalCode = postalCode
        self.annualIncome = annualIncome
        self.netAnnualIncome = netAnnualIncome
        self.shortTermSavings = shortTermSavings
        self.livingExpenses = livingExpenses
        self.agent = agent
        self.policies = policies
        self.lastPurchased = lastPurchased.toDate()
        self.nextContact = nextContact.toDate()!
    }
    
    func deleteClient() {
        let route = Globals.Routes.baseClientRoute + "\(id)"
        NetworkUtility.shared.HTTPDelete(route: route) { (response) in
            print(response)
        }
    }
    
    func getPremiumPayments(completion: @escaping (JSON) -> ()) {
        let route = Globals.Routes.getClientByAgentId + "\(id)"
        NetworkUtility.shared.HTTPGet(route: route) { (response) in
            completion(response)
        }
    }
    
    func getPolicies(completion: @escaping([PolicyModel]) -> ()) {
        let route = "\(Globals.Routes.baseClientRoute)\(id)/policies"
        NetworkUtility.shared.HTTPGet(route: route) { (response) in
            if !response["status"].exists() {
                guard let policies = response.array else {return}
                self.policyList.removeAll()
                for policy in policies {
                    self.policyList.append(PolicyModel.init(id: policy["_id"].stringValue, name: policy["name"].stringValue, issuerName: policy["issuerName"].stringValue, policyNumber: policy["policyNumber"].stringValue, policyStartDate: policy["policyStartDate"].stringValue, policyEndDate: policy["policyEndDate"].stringValue, policyStartAge: policy["policyStartAge"].intValue, policyEndAge: policy["policyEndAge"].intValue, policyDuration: policy["policyDuration"].intValue, paymentMode: policy["paymentMode"].stringValue, premiumEndAge: policy["premiumEndAge"].intValue, annualPremium: policy["annualPremium"].doubleValue, paymentFrequency: policy["paymentFrequency"].stringValue, policyType: policy["policyType"].stringValue, benefits: policy["benefits"], cashValueAge: Int(policy["cashValueAge"].doubleValue), cashValueAmount: policy["cashValueAmount"].doubleValue, couponStartDate: policy["couponStartDate"].stringValue, couponEndDate: policy["couponEndDate"].stringValue, couponAmount: policy["couponAmount"].doubleValue, remark: policy["remarks"].stringValue, client: policy["client"].intValue))
                }
                completion(self.policyList)
            }
        }
    }
    
    func updateClient(completion: @escaping (JSON) -> ()) {
        let url = Globals.shared.HOST_URL + Globals.Routes.baseClientRoute
        let parameters: [String:Any] = ["_id":id, "name":name, "clientType": clientType, "nric":nric, "prefName":prefName, "email":email, "dateOfBirth": dateOfBirth.toFormat("yyyy-MM-dd"), "gender":gender, "race":race, "familyOf":familyOf, "nationality":nationality, "remark":remark, "handphone":handphone, "address1":address1, "address2":address2, "postalCode":postalCode, "relationship":relationship, "shortTermSavings":shortTermSavings, "livingExpenses":livingExpenses, "annualIncome": annualIncome ?? 0, "netAnnualIncome": netAnnualIncome ?? 0, "nextContact": nextContact.toFormat("yyyy-MM-dd")]
        Alamofire.request(url, method:.put, parameters:parameters, encoding:URLEncoding.default).responseJSON { response in
            print(response)
            switch response.result {
            case .success(let json_value):
                let json = JSON(json_value)
                if json["result"].intValue == -1 {
                    Globals.shared.showDefaultAlert(title: "Error", message: json["message"].stringValue)
                }
                else {
                    completion(json)
                }
                
            case .failure(let error):
                print(error)
                Globals.shared.showDefaultAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    func populateFamilyOf() {
        if familyOf == "" {
            return
        }
        if familyOfName != "" {
            return
        }
        let route = Globals.Routes.baseClientRoute + "/\(familyOf)"
        NetworkUtility.shared.HTTPGet(route: route) { (response) in
            if response["Error"].exists() {
                
            }
            else {
                self.familyOfName = response["name"].stringValue
            }
        }
    }
}


//MARK: - ViewModel
class ClientViewModel {
    private let client: ClientModel
    public init (client: ClientModel) {
        self.client = client
    }
    
    
    public var name: String {
        get {
            return self.client.name.capitalized
        }
        set {
            self.client.name = newValue
        }
    }
    
    public var prefName: String {
        get {
            return self.client.prefName.capitalized
        }
        set {
            self.client.prefName = newValue
        }
    }
    
    public var clientType: String {
        get {
            return self.client.clientType.capitalized
        }
        set {
            self.client.clientType = newValue
        }
    }
    
    public var nric: String {
        get {
            return self.client.nric
        }
        set {
            self.client.nric = newValue
        }
    }
    
    public var email: String {
        get {
            return self.client.email
        }
        set {
            self.client.email = newValue
        }
    }
    
    public var gender: String {
        get {
            return self.client.gender
        }
        set {
            self.client.gender = newValue
        }
    }
    
    public var race: String {
        get {
            return self.client.race
        }
        set {
            self.client.race = newValue
        }
    }
    
    public var familyOf: String {
        get {
            return self.client.familyOf
        }
        
        set {
            self.client.familyOf = newValue
        }
    }
    
    public var relationship: String {
        get {
            return self.client.relationship
        }
        
        set {
            self.client.relationship = newValue
        }
    }
    
    public var familyOfName: String {
        get {
            return self.client.familyOfName
        }
    }
    
    public var nationality: String {
        get {
            return self.client.nationality
        }
        set {
            self.client.nationality = newValue
        }
    }
    
    public var handphone: String {
        get {
            return self.client.handphone
        }
        set {
            self.client.handphone = newValue
        }
    }
    
    public var address1: String {
        get {
            return self.client.address1
        }
        set {
            self.client.address1 = newValue
        }
    }
    
    public var address2: String {
        get {
            return self.client.address2
        }
        set {
            self.client.address2 = newValue
        }
    }
    
    public var postalCode: String {
        get {
            return self.client.postalCode
        }
        set {
            self.client.postalCode = newValue
        }
    }
    
    
    public var shortTermSavings: String {
        get {
            return "\(client.shortTermSavings)"
        }
        set {
            guard let savings = Double(newValue) else {return}
            self.client.shortTermSavings = savings
        }
    }
    
    public var livingExpenses: String {
        get {
            return "\(client.livingExpenses)"
        }
        set {
            guard let expenses = Double(newValue) else {return}
            self.client.livingExpenses = expenses
        }
    }
    
    public var remark: String {
        get {
            return self.client.remark
        }
        set {
            self.client.remark = newValue
        }
    }
    
    public var ageInYears: String {
        get {
            return "\(self.client.age) years old "
        }
        
        set {
            guard let clientAge = Int(newValue) else {return}
            self.client.age = clientAge
        }
    }
    
    public var annualIncomeSGD: String {
        guard let annualIncome = self.client.annualIncome else {return "$SGD 0"}
        return "$SGD \(Int(annualIncome).withCommas())"
    }
    
    public var annualIncomeValue: Double {
        get {
            guard let annualIncome = self.client.annualIncome else {return 0}
            return annualIncome
        }
        set {
            self.client.annualIncome = newValue
        }
    }
    
    public var netAnnualIncomeValue: Double {
        get {
            guard let netIncome = self.client.netAnnualIncome else {return 0}
            return netIncome
        }
        set {
            self.client.netAnnualIncome = newValue
        }
    }
    
    public var dateOfBirth: String {
        get {
            return self.client.dateOfBirth.toFormat("yyyy-MM-dd")
        }
        set {
            guard let date = newValue.toDate("yyyy-MM-dd") else {return}
            self.client.dateOfBirth = date
        }
    }
    
    public var lastPurchased: String {
        get {
            guard let lastPurchased = self.client.lastPurchased else {return "No purchases"}
            return lastPurchased.toFormat("dd MMM yyyy")
        }
    }
    
    public var nextContact: String {
        get {
            return self.client.nextContact.toFormat("yyyy-MM-dd")
        }
        set {
            guard let date = newValue.toDate("yyyy-MM-dd") else {return}
            self.client.nextContact = date

        }
    }
    
    public func getPolices(completion: @escaping ([PolicyModel]) -> ()) {
        self.client.getPolicies { (response) in
            completion(response)
        }
    }
    
    public func updateClient(completion: @escaping (JSON) -> ()) {
        self.client.updateClient { (response) in
            completion(response)
        }
    }
    
    public func populateFamilyOf() {
        self.client.populateFamilyOf()
    }
}

