//
//  Notification.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 18/1/19.
//  Copyright © 2019 Bilguun. All rights reserved.
//

import Foundation
import SwiftDate

struct NotificationModel {
    var unread: Bool
    var createdAt: DateInRegion?
    var senderId: Int
    var sender: String
    var subject: String
    var body: String
}
