//
//  Notification.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 18/1/19.
//  Copyright © 2019 Bilguun. All rights reserved.
//

import Foundation

struct NotificationModel {
    let name: String?
    let time: String?
    let location: String?
    let date: String?
}
