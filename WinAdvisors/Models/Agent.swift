//
//  Agents.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 8/6/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Agent {
    
    var id: String = ""
    var name: String = ""
    var email: String = ""
    var googleUserId: String = ""
    var idToken: String = ""
    
    var agentPreferences: AgentPreferences = .init(riskColor: .red, wealthColor: .blue, legacyColor: .green, deathOpt: 10, critIllOpt: 10, accDeathOpt: 100000)
    
    var clients = [ClientModel]()
    
    static let current = Agent()
    
    private init() {}
    
    func login(name: String, email: String, idToken: String, completion: @escaping (JSON) -> ()) {
        let url = Globals.shared.HOST_URL + Globals.Routes.agentLogin
        let headers = ["x-auth-token": idToken]
        print(url)
        let parameters = ["name":name, "email":email]
        Alamofire.request(url, method:.post, parameters:parameters, encoding:URLEncoding.default, headers: headers).responseJSON { response in
            print(response)
            switch response.result {
            case .success(let json_value):
                let json = JSON(json_value)
                self.setAgent(id: json["_id"].stringValue, name: json["name"].stringValue, email: json["email"].stringValue, googleUserId: json["googleUserId"].stringValue, idToken: idToken)
                completion(json)
            case .failure(let error):
                print(error)
                completion(JSON.null)
            }
        }
    }
    
    func setAgent(id: String, name: String, email: String, googleUserId: String, idToken: String) {
        self.id = id
        self.name = name
        self.email = email
        self.googleUserId = googleUserId
        self.idToken = idToken
    }
    
    func addNewClient(parameters: inout [String:String], completion: @escaping (JSON) -> ()) {
        let url = Globals.shared.HOST_URL + Globals.Routes.baseClientRoute
        parameters.updateValue(String.init(describing: id), forKey: "agent")
        Alamofire.request(url, method:.post, parameters:parameters, encoding:URLEncoding.default).responseJSON { response in
            print(response)
            switch response.result {
            case .success(let json_value):
                let json = JSON(json_value)
                completion(json)
            case .failure(let error):
                print(error)
                completion(JSON(error))
            }
        }
    }
    
    func searchClientNames(text: String) -> [ClientModel] {
        let clients = self.clients.filter { (client) -> Bool in
            return client.name.lowercased().contains(text)
        }
        
        return clients
    }
    
    
    func updatePreferences() {
        if let savedPrefs = UserDefaults.standard.object(forKey: "Preferences") as? Data {
            let decoder = JSONDecoder()
            if let loadedPrefs = try? decoder.decode(SavedPreferences.self, from: savedPrefs) {
                print(loadedPrefs)
                agentPreferences = AgentPreferences.init(riskColor: UIColor.UIColorFromString(string: loadedPrefs.riskColorString), wealthColor: UIColor.UIColorFromString(string: loadedPrefs.wealthColorString), legacyColor: UIColor.UIColorFromString(string: loadedPrefs.legacyColorString), deathOpt: loadedPrefs.deathTpdOptValue, critIllOpt: loadedPrefs.critIllOptValue, accDeathOpt: loadedPrefs.accDeathTpdOptValue)
            }
        }
    }
}

struct AgentPreferences {
    var riskColor: UIColor
    var wealthColor: UIColor
    var legacyColor: UIColor
    var deathOpt: Double
    var critIllOpt: Double
    var accDeathOpt: Double
}
