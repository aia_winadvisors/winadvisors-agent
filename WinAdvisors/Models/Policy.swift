//
//  Policies.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 8/6/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import Foundation
import SwiftDate
import SwiftyJSON

//MARK: - Model
public class PolicyModel {
    var id: String
    var name: String
    var issuerName: String
    var policyNumber: String
    var policyStartDate: DateInRegion
    var policyEndDate: DateInRegion
    var policyStartAge: Int
    var policyEndAge: Int
    var policyDuration: Int
    var paymentMode: String
    var premiumEndAge: Int
    var annualPremium: Double
    var paymentFrequency: String
    var policyType: String
    var benefits: JSON
    var remark: String
    var cashValueAge: Int
    var cashValueAmount: Double
    var couponStartDate: DateInRegion?
    var couponEndDate: DateInRegion?
    var couponAmount: Double
    var client: Int
    
    
    init(id:String, name: String, issuerName:String, policyNumber: String, policyStartDate: String, policyEndDate: String, policyStartAge: Int, policyEndAge: Int, policyDuration: Int, paymentMode: String, premiumEndAge: Int, annualPremium:Double, paymentFrequency: String, policyType:String, benefits: JSON, cashValueAge: Int, cashValueAmount: Double, couponStartDate: String, couponEndDate: String, couponAmount: Double, remark: String, client: Int) {
        self.id = id
        self.name = name
        self.issuerName = issuerName
        self.policyNumber = policyNumber
        self.policyStartDate = policyStartDate.toDate()!
        self.policyEndDate = policyEndDate.toDate()!
        self.policyStartAge = policyStartAge
        self.policyEndAge = policyEndAge
        self.policyDuration = policyDuration
        self.paymentMode = paymentMode
        self.premiumEndAge = premiumEndAge
        self.annualPremium = annualPremium
        self.paymentFrequency = paymentFrequency
        self.policyType = policyType
        self.benefits = benefits
        self.remark = remark
        self.client = client
        self.cashValueAge = cashValueAge
        self.cashValueAmount = cashValueAmount
        self.couponStartDate = couponStartDate.toDate()
        self.couponEndDate = couponEndDate.toDate()
        self.couponAmount = couponAmount
    }
}

//MARK: - ViewModel
public class PolicyViewModel {
    private let policy: PolicyModel
    
    public init(policy: PolicyModel) {
        self.policy = policy
    }
    
    public var id: String {
        get {
            return self.policy.id
        }
    }
    
    public var name: String {
        get {
            return self.policy.name
        }
        set {
            self.policy.name = newValue
        }
    }
    
    public var issuerName: String {
        get {
            return self.policy.issuerName
        }
    }
    
    public var policyNumber: String {
        get {
            return self.policy.policyNumber
        }
    }
    
    public var policyStartDate: String {
        get {
            return self.policy.policyStartDate.toFormat("yyyy-MM-dd")
        }
    }
    
    public var policyEndDate: String {
        get {
            return self.policy.policyEndDate.toFormat("yyyy-MM-dd")
        }
    }
    
    public var policyStartAge: String {
        get {
            return "\(self.policy.policyStartAge)"
        }
    }
    
    public var policyEndAge: String {
        get {
            return "\(self.policy.policyEndAge)"
        }
    }
    
    public var premiumEndAge: String {
        get {
            return "\(self.policy.premiumEndAge)"
        }
    }
    
    public var policyDuration: String {
        get {
            return "\(self.policy.policyDuration)"
        }
    }
    
    public var paymentMode: String {
        get {
            return self.policy.paymentMode
        }
    }
    
    public var premium: String {
        get {
            var premium = 0.0
            if self.policy.paymentFrequency == "MONTHLY" {
                premium = self.policy.annualPremium / 12
            }
            else {
                premium = self.policy.annualPremium
            }
            return String.init(format: "%.2f", premium)
        }
    }
    
    public var paymentFrequency: String {
        get {
            return self.policy.paymentFrequency
        }
    }
    
    public var cashValueAge: String {
        get {
            return "\(self.policy.cashValueAge)"
        }
    }
    
    public var cashValueAmount: String {
        get {
            return "\(self.policy.cashValueAmount)"
        }
    }
    
    public var couponStartDate: String {
        get {
            guard let startDate = self.policy.couponStartDate else {return ""}
            return startDate.toFormat("yyyy-MM-dd")
        }
    }
    
    public var couponEndDate: String {
        get {
            guard let endDate = self.policy.couponEndDate else {return ""}
            return endDate.toFormat("yyyy-MM-dd")
        }
    }
    
    public var couponAmount: String {
        get {
            return "\(self.policy.couponAmount)"
        }
    }
    
    public var benefits: [[String:String]] {
        get {
            var benefitsArray = [[String:String]]()
            for (_, subjson):(String, JSON) in self.policy.benefits {
                benefitsArray.append([
                    "name": subjson["name"].stringValue,
                    "type" : subjson["type"].stringValue,
                    "value" : subjson["value"].stringValue
                    ])
            }
            return benefitsArray
        }
    }
    
    public var policyType: String {
        get {
            switch self.policy.policyType {
            case "risk":
                return "RISK MANAGEMENT"
            case "wealth_mgmt":
                return "WEALTH MANAGEMENT (LEGACY)"
            case "wealth_accum":
                return "WEALTH ACCUMULATION"
            default:
                return "RISK MANAGEMENT"
            }
        }
    }
    
    public var remark: String {
        get {
            return self.policy.remark
        }
    }
    
}


