//
//  Benefits.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 2/1/19.
//  Copyright © 2019 Bilguun. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Benefits {
    var death: String = "S$0"
    var deathMet: Bool = false
    var deathShortFall: String = "No Policy"
    var tpd: String = "S$0"
    var tpdMet: Bool = false
    var tpdShortFall: String = "No Policy"
    var critIll: String = "S$0"
    var critIllMet: Bool = false
    var critIllShortFall: String = "No Policy"
    var earlyCritIll: String = "S$0"
    var earlyCritIllMet: Bool = false
    var earlyCritIllShortFall: String = "No Policy"
    var disabilityIncome: String = "S$0"
    var disabilityIncomeMet: Bool = false
    var disabilityIncomeShortFall: String = "No Policy"
    var hosp: String = "None"
    var hospMet: Bool = true
    var hospShortFall: String = "No Policy"
    var hospIncome: String = "None"
    var hospIncomeShortFall: String = "No Policy"
    var hospIncomeMet: Bool = true
    var accDeath: String = "S$0"
    var accDeathShortFall: String = "No Policy"
    var accDeathMet: Bool = false
    var accTpd: String = "S$0"
    var accTpdShortFall: String = "No Policy"
    var accTpdMet: Bool = false
    var accReimbursement: String = "S$0"
    var accReimbursementShortFall: String = "No Policy"
    var accReimbursementMet: Bool = false
    
    init(benefits: [JSON], annualIncome: Double) {
        for benefit in benefits {
            let benefitValue = benefit["totalValue"].doubleValue
            switch benefit["benefitType"].stringValue {
            case BenefitType.death.rawValue:
                death = "S$\(benefit["totalValue"].intValue.withCommas())"
                deathMet = benefitValue == 0 ? false : (annualIncome * Agent.current.agentPreferences.deathOpt <= benefitValue)
                deathShortFall = (benefitValue - annualIncome * Agent.current.agentPreferences.deathOpt) < 0 ? "Short fall: \(Int(abs(benefitValue - annualIncome  * Agent.current.agentPreferences.deathOpt)).withCommas())" : "No shortfall"
            case BenefitType.tpd.rawValue:
                tpd = "S$\(benefit["totalValue"].intValue.withCommas())"
                tpdMet = benefitValue == 0 ? false : (annualIncome * Agent.current.agentPreferences.deathOpt <= benefitValue)
                tpdShortFall = (benefitValue - annualIncome * Agent.current.agentPreferences.deathOpt) < 0 ? "Short fall: \(Int(abs(benefitValue - annualIncome  * Agent.current.agentPreferences.deathOpt)).withCommas())" : "No shortfall"
            case BenefitType.crit_ill.rawValue:
                critIll = "S$\(benefit["totalValue"].intValue.withCommas())"
                critIllMet = benefitValue == 0 ? false : (annualIncome * Agent.current.agentPreferences.critIllOpt <= benefitValue)
                critIllShortFall = (benefitValue - annualIncome * Agent.current.agentPreferences.critIllOpt) < 0 ? "Short fall: \(Int(abs(benefitValue - annualIncome  * Agent.current.agentPreferences.critIllOpt)).withCommas())" : "No shortfall"
            case BenefitType.early_crit_ill.rawValue:
                earlyCritIll = "S$\(benefit["totalValue"].intValue.withCommas())"
                earlyCritIllMet = benefitValue == 0 ? false : (annualIncome * Agent.current.agentPreferences.critIllOpt <= benefitValue)
                earlyCritIllShortFall = (benefitValue - annualIncome * Agent.current.agentPreferences.critIllOpt) < 0 ? "Short fall: \(Int(abs(benefitValue - annualIncome  * Agent.current.agentPreferences.critIllOpt)).withCommas())" : "No shortfall"
            case BenefitType.disability_income.rawValue:
                disabilityIncome = "S$\(benefit["totalValue"].intValue.withCommas())"
                disabilityIncomeMet = benefitValue == 0 ? false : (benefitValue > annualIncome / 12 * 0.7)
                disabilityIncomeShortFall = (benefitValue  - annualIncome / 12 * 0.7) < 0 ? "Short fall: \(Int(abs(benefitValue - annualIncome / 12 * 0.7)).withCommas())" : "No shortfall"
            case BenefitType.hosp.rawValue:
                hosp = benefit["remarks"].stringValue
            case BenefitType.hosp_income.rawValue:
                hospIncome = benefit["remarks"].stringValue
            case BenefitType.acc_death.rawValue:
                accDeath = "S$\(benefit["totalValue"].intValue.withCommas())"
                accDeathMet = benefitValue == 0 ? false : (benefitValue > Agent.current.agentPreferences.accDeathOpt)
                accDeathShortFall = (benefitValue < Agent.current.agentPreferences.accDeathOpt) ? "Short fall: \(Int(Agent.current.agentPreferences.accDeathOpt - benefitValue).withCommas())" : "No shortfall"
            case BenefitType.acc_tpd.rawValue:
                accTpd = "S$\(benefit["totalValue"].intValue.withCommas())"
                accTpdMet = benefitValue == 0 ? false : (benefitValue > Agent.current.agentPreferences.accDeathOpt)
                accTpdShortFall = (benefitValue < Agent.current.agentPreferences.accDeathOpt) ? "Short fall: \(Int(Agent.current.agentPreferences.accDeathOpt - benefitValue).withCommas())" : "No shortfall"
            case BenefitType.acc_reimbursement.rawValue:
                accReimbursement = "S$\(benefit["totalValue"].intValue.withCommas())"
                accReimbursementMet = benefitValue == 0 ? false : (benefitValue > 2000)
                accReimbursementShortFall = (benefitValue < 2000) ? "Short fall: \(Int(2000 - benefitValue).withCommas())" : "No shortfall"
            default:
                print("Default")
            }
        }
    }
}

struct BenefitsViewModel {
    
    private var benefits: Benefits
    
    init(benefits: Benefits) {
        self.benefits = benefits
    }
}

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}

enum BenefitType: String {
    case death = "death"
    case tpd = "tpd"
    case crit_ill = "crit_ill"
    case early_crit_ill = "early_crit_ill"
    case disability_income = "disability_income"
    case hosp = "hosp"
    case hosp_income = "hosp_income"
    case acc_death = "acc_death"
    case acc_tpd = "acc_tpd"
    case acc_reimbursement = "acc_reimbursement"
}
