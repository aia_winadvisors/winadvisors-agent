//
//  NetworkUtility.swift
//  Client
//
//  Created by Bilguun Batbold on 15/9/18.
//  Copyright © 2018 Win Advisors. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SVProgressHUD

class NetworkUtility {
    
    static let shared = NetworkUtility()
    private init() {}
    
    //MARK: - HTTP Requests
    
    
    func HTTPGet(route:String, completion: @escaping (JSON) -> ()) {
        let url = Globals.shared.HOST_URL + route
        Alamofire.request(url, method:.get, encoding:URLEncoding.default).responseJSON { response in
            print(response)
            if response.response?.statusCode != 200 {
                let value:JSON = ["Error": response.response?.statusCode as Any]
                completion(value)
            }
            switch response.result {
            case .success(let json_value):
                let value = JSON(json_value)
                completion(value)
            case .failure(let error):
                let value:JSON = ["Error": error._code]
                completion(value)
            }
        }
    }
    
    func HTTPDelete(route:String, completion: @escaping (JSON) -> ()) {
        let url = Globals.shared.HOST_URL + route
        Alamofire.request(url, method:.delete, encoding:URLEncoding.default).responseJSON { response in
            print(response)
            switch response.result {
            case .success(let json_value):
                let value = JSON(json_value)
                completion(value)
            case .failure(let error):
                let value:JSON = ["Error": error._code]
                completion(value)
            }
        }
    }
    
    func HTTPPost(route:String, parameters: [String:Any], completion: @escaping (JSON) -> ()) {
        let url = Globals.shared.HOST_URL + route
        Alamofire.request(url, method:.post, parameters:parameters, encoding:JSONEncoding.default).responseJSON { response in
            print(response)
            if response.response?.statusCode != 200 {
                let value:JSON = ["Error": "API Error"]
                completion(value)
            }
            switch response.result {
            case .success(let json_value):
                let value = JSON(json_value)
                completion(value)
            case .failure(let error):
                let value:JSON = ["Error": error._code]
                completion(value)
            }
        }
    }
    
    func HTTPPut(route:String, parameters: [String:Any], completion: @escaping (JSON) -> ()) {
        let url = Globals.shared.HOST_URL + route
        Alamofire.request(url, method:.put, parameters:parameters, encoding:JSONEncoding.default).responseJSON { response in
            print(response)
            if response.response?.statusCode != 200 {
                let value:JSON = ["Error": "API Error"]
                completion(value)
            }
            switch response.result {
            case .success(let json_value):
                let value = JSON(json_value)
                completion(value)
            case .failure(let error):
                let value:JSON = ["Error": error._code]
                completion(value)
            }
        }
    }
}
