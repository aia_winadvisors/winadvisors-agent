//
//  Datacontroller.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 31/8/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import Foundation

class Globals {
    
    static let shared = Globals()
    private init() {}
    
    var MainNavController:UINavigationController? = nil
    
//    var HOST_URL = "http://127.0.0.1:3000"
    //    var HOST_URL = "http://192.168.1.129:3000"
        var HOST_URL = "https://aia.bilguun-apps.ml"
    
    
    struct Routes {
        static var baseAgentRoute = "/api/agents/"
        static var agentLogin = "/api/agents/login"
        static var agentGetClients = "/api/agents/clients"
        static var getAgentByEmail = "\(baseAgentRoute)getagentbyemail/"
        static var baseClientRoute = "/api/clients/"
        static var basePoliciesRoute = "/api/policies"
        static var getClientByAgentId = "\(baseClientRoute)getClientsByAgentId/"
        static var addNewClientPolicy = "\(baseClientRoute)newPolicy"
        static var addNewClient = "\(baseClientRoute)newClient"
        static var getPoliciesByClientId = "\(baseClientRoute)getPoliciesByClientId/"
        static var deleteClientById = "\(baseClientRoute)deleteClientById/"
        static var getClientPremiumsByClientId = "\(baseClientRoute)getAnnualPremiumsByClientId/"
        
        static var basePolicyRoute = "/api/policy/"
        
        static var baseNotificationRoute = "/api/notifications/"
        static var getAgentNotifications = "/api/notifications/agent/"
        
    }
    
    var CurrentSideMenu: SideMenus = .Home
    
    enum SideMenus {
        case Home, Clients, Policies, Notification, Settings
    }
    
    //MARK: - Show alert
    //shows alert from the existing view controller
    func showDefaultAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        UIApplication.shared.windows.last?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}
