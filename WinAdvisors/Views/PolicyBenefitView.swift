//
//  PolicyBenefitView.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 15/9/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//

import UIKit
import Foundation
import MaterialComponents

class PolicyBenefitView: UIView {
    @IBOutlet weak var benefitNameTextField: MDCTextField!
    @IBOutlet weak var percentageCoverageTextField: MDCTextField!
    @IBOutlet weak var cashCoverageTextField: MDCTextField!
    @IBOutlet weak var remarkTextField: MDCTextField!
    
    // MARK: Properties
    
    var benefitNameController: MDCTextInputControllerOutlined?
    var percentageCoverageController: MDCTextInputControllerOutlined?
    var cashCoverageController: MDCTextInputControllerOutlined?
    var remarkController: MDCTextInputControllerOutlined?
    
    func setup() {
        benefitNameController = MDCTextInputControllerOutlined(textInput: benefitNameTextField)
        percentageCoverageController = MDCTextInputControllerOutlined(textInput: percentageCoverageTextField)
        cashCoverageController = MDCTextInputControllerOutlined(textInput: cashCoverageTextField)
        remarkController = MDCTextInputControllerOutlined(textInput: remarkTextField)
    }
    
}
