//
//  IncomeRatioView.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 19/2/19.
//  Copyright © 2019 Bilguun. All rights reserved.
//

import Foundation
class IncomeRatioView: UIView {
    
    @IBOutlet weak var contentView: UIView!
//    @IBOutlet weak var medicalHealthProgressView: UIProgressView!
//    @IBOutlet weak var medicalHealthPercentage: UILabel!
    @IBOutlet weak var insuranceCoverProgressView: UIProgressView!
    @IBOutlet weak var insuranceCoverPercentage: UILabel!
    @IBOutlet weak var wealthAccumulationProgressView: UIProgressView!
    @IBOutlet weak var wealthAccumulationPercentage: UILabel!
    @IBOutlet weak var shortTermSavingsProgressView: UIProgressView!
    @IBOutlet weak var shortTermSavingsPercentage: UILabel!
    @IBOutlet weak var livingExpensesProgressView: UIProgressView!
    @IBOutlet weak var livingExpensesPercentage: UILabel!
    @IBOutlet weak var unusedIncome: UILabel!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        Bundle.main.loadNibNamed("IncomeRatioView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
    }
    
    public func configure(incomeRatio: IncomeRatio) {
//        medicalHealthProgressView.progress = Float(incomeRatio.health / (incomeRatio.annualIncome * 0.05))
//        medicalHealthPercentage.text = "\(String.init(format: "%.2f", (incomeRatio.health * 100 / incomeRatio.annualIncome)))%"
        insuranceCoverProgressView.progress = Float(incomeRatio.insuranceCover / (incomeRatio.annualIncome * 0.15))
        insuranceCoverPercentage.text = "\(String.init(format: "%.2f", (incomeRatio.insuranceCover * 100 / incomeRatio.annualIncome)))%"
        insuranceCoverProgressView.tintColor = Agent.current.agentPreferences.riskColor
        insuranceCoverPercentage.textColor = Agent.current.agentPreferences.riskColor
        
        wealthAccumulationProgressView.progress = Float(incomeRatio.wealthAccumulation / (incomeRatio.annualIncome * 0.15))
        wealthAccumulationPercentage.text = "\(String.init(format: "%.2f", (incomeRatio.wealthAccumulation * 100 / incomeRatio.annualIncome)))%"
        wealthAccumulationProgressView.tintColor = Agent.current.agentPreferences.wealthColor
        wealthAccumulationPercentage.textColor = Agent.current.agentPreferences.wealthColor
        
        shortTermSavingsProgressView.progress = Float(incomeRatio.shortTermSavings / (incomeRatio.annualIncome * 0.20))
        shortTermSavingsPercentage.text = "\(String.init(format: "%.2f", (incomeRatio.shortTermSavings * 100 / incomeRatio.annualIncome)))%"
        livingExpensesProgressView.progress = Float(incomeRatio.livingExp / (incomeRatio.annualIncome * 0.50))
        livingExpensesPercentage.text = "\(String.init(format: "%.2f", (incomeRatio.livingExp * 100 / incomeRatio.annualIncome)))%"
        unusedIncome.text = "Unused income is: \(String.init(format: "%.2f", (incomeRatio.unusedIncome * 100 / incomeRatio.annualIncome)))%"

    }
    
}
