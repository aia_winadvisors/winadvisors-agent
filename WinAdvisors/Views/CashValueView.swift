//
//  CashValueView.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 15/9/18.
//  Copyright © 2018 Bilguun. All rights reserved.
//


import UIKit
import Foundation
import MaterialComponents

class CashValueView: UIView {
    @IBOutlet weak var ageTextField: MDCTextField!
    @IBOutlet weak var cashValueTextField: MDCTextField!
    
    // MARK: Properties
    
    var ageController: MDCTextInputControllerOutlined?
    var cashValueController: MDCTextInputControllerOutlined?

    func setup() {
        ageController = MDCTextInputControllerOutlined(textInput: ageTextField)
        cashValueController = MDCTextInputControllerOutlined(textInput: cashValueTextField)
    }
    
}
