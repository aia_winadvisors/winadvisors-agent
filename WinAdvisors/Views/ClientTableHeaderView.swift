//
//  ClientTableHeaderView.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 12/5/19.
//  Copyright © 2019 Bilguun. All rights reserved.
//

import Foundation

class ClientTableHeaderView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    
    weak var delegate: ClientTableHeaderViewProtocol?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        let name = String(describing: type(of: self))
        let nib = UINib(nibName: name, bundle: .main)
        nib.instantiate(withOwner: self, options: nil)
        
        // next: append the container to our view
        self.addSubview(self.contentView)
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.contentView.topAnchor.constraint(equalTo: self.topAnchor),
            self.contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            self.contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            ])
    }
    
    @IBAction func sortRowsTap(_ sender: UIButton) {
        delegate?.sortBy(option: SortClientBy.init(rawValue: sender.tag) ?? SortClientBy.name)
    }
}

protocol ClientTableHeaderViewProtocol: class {
    func sortBy(option: SortClientBy)
}

enum SortClientBy: Int {
    case name = 0
    case lastPurchaseDate
    case nextContactDate
    case birthday
}
