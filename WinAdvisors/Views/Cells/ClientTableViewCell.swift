//
//  ClientCell.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 20/2/19.
//  Copyright © 2019 Bilguun. All rights reserved.
//

import Foundation

class ClientTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nextContactDate: UILabel!
    @IBOutlet weak var lastPurchaseDateLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configure(with model: ClientModel) {
        nameLabel.text = model.name
        nextContactDate.text = model.nextContact.toFormat("dd MMMM YYYY")
        birthdayLabel.text = model.dateOfBirth.toFormat("dd MMM YYYY")
        guard let lastPurchased = model.lastPurchased else {
            lastPurchaseDateLabel.text = "No Purchases"
            return
        }
        
        lastPurchaseDateLabel.text = lastPurchased.toFormat("dd MMM YYYY")
    }
}
