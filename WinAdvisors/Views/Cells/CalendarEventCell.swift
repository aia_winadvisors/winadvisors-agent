//
//  CalendarEventCell.swift
//  WinAdvisors
//
//  Created by Bilguun Batbold on 17/2/19.
//  Copyright © 2019 Bilguun. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents.MaterialTypography

class CalendarEventCell: UITableViewCell {
    @IBOutlet weak var weekDay: UILabel!
    @IBOutlet weak var monthDay: UILabel!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.eventName.font = MDCTypography.subheadFont()
        self.eventName.textColor = UIColor.white
        self.eventTime.font = MDCTypography.subheadFont()
        self.eventTime.textColor = UIColor.white
        self.weekDay.font = MDCTypography.subheadFont()
        self.weekDay.textColor = UIColor.black
        self.monthDay.font = MDCTypography.subheadFont()
        self.monthDay.textColor = UIColor.black
    }
    
    public func configure(with model: CalendarEventModel) {
        weekDay.text = model.weekDay
        monthDay.text = model.monthDay
        eventName.text = model.eventName
        var eventTime: String = ""
        eventTime += model.eventStartTime
        if model.eventEndTime != "" {
            eventTime += " -" + model.eventEndTime
        }
        if model.eventLocation != "" {
            eventTime += " at " + model.eventLocation
        }
        self.eventTime.text = eventTime
    }
}

struct CalendarEventModel {
    let eventName: String
    let eventStartTime: String
    let eventEndTime: String
    let eventLocation: String
    let date: String
    let month: String
    let monthDay: String
    let weekDay: String
}
